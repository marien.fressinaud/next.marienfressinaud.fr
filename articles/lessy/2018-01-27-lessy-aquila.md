Date: 2018-01-27 17:35
Title: Nouvelle version de Lessy : Aquila
Slug: lessy-aquila
Tags:
Summary: À chaque mois sa nouvelle version de Lessy, cette fois-ci la version Aquila apporte un mécanisme pour synchroniser le navigateur en temps réel si des changements surviennent sur le serveur.

J’ai désormais adopté un petit rythme de croisière quant aux sorties des
nouvelles versions de Lessy et je peux donc annoncer la sortie de la version
Aquila.

Pour rappel, Lessy est un gestionnaire de temps destiné à vous aider à mieux
vous organiser en associant à vos tâches des indicateurs clairs sur ce que vous
avez de plus urgent à réaliser (ou abandonner) dans l’immédiat. Un service est
mis à disposition gratuitement sur [lessy.yuzu.ovh](https://lessy.yuzu.ovh) et le code est
[hébergé sur GitHub](https://github.com/marienfressinaud/lessy), le tout sous
[licence libre](https://github.com/marienfressinaud/lessy/blob/master/LICENSE).

Cette nouvelle version n’apporte pas énormément de nouvelles fonctionnalités
mais le mois de janvier a tout de même été chargé pour moi.

Tout d’abord, l’amélioration phare de cette nouvelle version est l’arrivée d’un
mécanisme pour synchroniser différents navigateurs : concrêtement, si vous
modifiez une tâche dans un navigateur, cette modification sera aussitôt
répercutée si vous avez ouvert Lessy dans un autre navigateur. Cette
fonctionnalité est encore expérimentale et elle n’est pas aussi aboutie que ce
que je souhaiterais, mais elle impliquait un gros changement dans
l’infrastructure du projet (un serveur Redis est maintenant nécessaire). Je
souhaitais donc m’en occuper avant d’aller plus loin dans le développement.

Ce qui m’a le plus occupé au final est le soudain coup de projecteur qu’a reçu
le projet au début du mois avec la parution d’[un article chez Korben](https://korben.info/lessy-gestionnaire-de-temps-ethique-respectueux.html).
Celui-ci n’a pas eu pour effet de faire tomber mon serveur (ouf !) mais a
permis une augmentation du nombre de retours ainsi qu’une apparition temporaire
dans le *trending* Ruby de GitHub. Les retours que j’ai eu ont été
particulièrement intéressants puisqu’ils m’ont fait réaliser que je suis encore
très loin d’avoir l’outil idéal que je souhaite développer et qu’il y a encore
beaucoup à faire malgré le travail réalisé jusqu’à maintenant. Les efforts à
fournir vont aussi bien se situer au niveau des fonctionnalités, de l’ergonomie
ou encore de l’accueil au sein de la communauté.

Je suis tout de même ravi qu’au bout de seulement un an le projet obtienne un
tel coup de projecteur, ça avait été plus compliqué pour [FreshRSS](https://freshrss.org)
par exemple (qui par contre vit très bien aujourd’hui sans mon implication).
J’espère que l’intérêt manifesté saura se transformer en contributions actives
et régulières ! Merci au passage à toutes celles et tous ceux qui m’ont fait
des retours et/ou ont contribué d’une façon ou d’une autre (commentaires dans
les tickets GitHub, ouverture de *pull requests*, etc.)

La prochaine version verra l’apparition d’une administration afin de me
permettre de mieux surveiller ce qu’il se passe sur le serveur. Aujourd’hui il
y a quelques 2200 comptes sur lessy.yuzu.ovh, dont la moitié ont été activés. J’en
profite pour rappeler l’existence d’[une page sur Liberapay](https://liberapay.com/Lessy)
si vous souhaitez participer aux frais du serveur (merci au passage aux
trois valeureux donateurs actuels).

J’ai aussi ouvert [un espace sur Framateam](https://framateam.org/lessy/channels/town-square)
si vous souhaitez venir discuter du projet directement avec moi. J’aimerais que
les échanges sur celui-ci se déroulent essentiellement en anglais puisque c’est
la langue par défaut que j’ai choisi pour le projet, mais un canal pour les
Français a été ouvert donc n’hésitez pas à venir et vous présenter !

Pour terminer, je remercie [Gavy](https://social.tcit.fr/@Gavy) pour avoir
ouvert une notice [sur Framalibre à propos de Lessy](https://framalibre.org/content/lessy),
non seulement ça permet de faire vivre ce projet historique de Framasoft, mais
en plus ça permet de faire encore un peu plus connaître Lessy :).
