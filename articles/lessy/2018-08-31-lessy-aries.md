Date: 2018-08-30 17:45
Title: Nouvelle version de Lessy : Aries
Slug: lessy-aries
Tags:
Summary: Lessy a connu une période de ralentissement dans son développement due à tout un tas de raisons, mais a quand même un peu évolué. Au menu : une administration, des conditions générales d’utilisation et la possibilité d’interdire les inscriptions.

Lessy a connu une période de ralentissement ces derniers mois, due à des
raisons externes (fonctionnement qui me correspond globalement bien, des choses
plus urgentes sur le feu et petit coup de fatigue). Cette pause dans le
développement m’a tout de même permis de prendre un peu de recul sur l’outil et
me permettra ainsi de réorienter certaines fonctionnalités qui existent
aujourd’hui… peut-être. Autant ne pas trop s’avancer sur le sujet.

Pour rappel, Lessy est un gestionnaire de temps destiné à vous aider à mieux
vous organiser en associant à vos tâches des indicateurs clairs sur ce que vous
avez de plus urgent à réaliser (ou abandonner) dans l’immédiat. Un service est
mis à disposition gratuitement sur [lessy.yuzu.ovh](https://lessy.yuzu.ovh) et le code est
[hébergé sur GitHub](https://github.com/marienfressinaud/lessy), le tout sous
[licence libre](https://github.com/marienfressinaud/lessy/blob/master/LICENSE).

![Dashboard de Lessy](images/lessy/2017-11-26-apus-dashboard.png)

Malgré le ralentissement évoqué plus haut, j’ai tout de même avancé un petit
peu sur quelques fonctionnalités, notamment&nbsp;:

- une administration basique&nbsp;;
- la possibilité de bloquer les inscriptions&nbsp;;
- l’arrivée de conditions d’utilisation.

Cette dernière fonctionnalité m’a pris plus de temps que prévu, mais je suis
assez fier de ce que j’ai fait. Les conditions en elles-mêmes sont accessibles
sur [lessy.yuzu.ovh](https://lessy.yuzu.ovh/terms-of-service). En résumé&nbsp;: je fais du
mieux que je peux, mais je ne m’engage ni sur la qualité, ni sur la pérennité
du service (je ne fais pas ça dans un cadre professionnel). Je m’engage
évidemment à ne pas toucher à vos données personnelles ni à les revendre.

Pour celles et ceux qui avaient souhaité s’inscrire alors que j’avais fermé les
inscriptions, ces conditions d’utilisation marquent la réouverture du service.
Notez toutefois que je risque de les refermer au-delà d’un certain nombre
d’inscrit·es (mais je ferai du tri avant dans les comptes non activés ou dans
ceux qui n’auront pas accepté les conditions d’utilisation d’ici 6 mois).

Les prochains développements porteront sur un espace pour modifier son profil
et sur la possibilité de choisir la langue de l’interface (enfin&nbsp;!). Vous
pouvez suivre l’avancement sur [le projet GitHub](https://github.com/lessy-community/lessy/projects/9).

Je remercie encore une fois celles et ceux qui parlent de Lessy autour
d’elleux, qui l’utilisent ou qui me suggèrent des améliorations. C’est aussi
ça, contribuer&nbsp;:). Et merci à Sarah pour avoir pris le temps de relire mes
conditions d’utilisation et Erwan pour… avoir essayé&nbsp;;).
