Date: 2017-10-31 13:30
Title: Lessy Antlia
Slug: lessy-antlia
Tags:
Summary: Lessy sort dans une nouvelle version&nbsp;: Antlia. Rien de neuf si ce n’est de la documentation en tout genre.

J’expliquais dans [mon article du 8 octobre](https://marienfressinaud.fr/project-zero-devient-lessy.html)
que je souhaitais que Lessy soit un projet s’adressant à une large portion de
la population non-geek. Pour cela, je voulais le rendre plus accueillant et
plus communautaire. C’est pourquoi je suis très fier d’annoncer la sortie de
Lessy Antlia (le nom de cette nouvelle version). Au menu, énormément de
documentation.

J’ai passé les dernières semaines à travailler les valeurs que je voulais
défendre avec Lessy. Après avoir dessiné, trié et organisé toutes les idées que
j’avais en tête, j’ai abouti à une première version de la vision de Lessy (en
anglais)&nbsp;:

> Lessy is a time manager application built upon strong principles. It is
> designed to be as **intuitive and inclusive** as possible. Its goal is to
> return people their **power to manage time**, instil **self-confidence** and
> challenge their **capacity to question themselves**. It is built by a
> **diverse and welcoming community**.

On peut traduire cela par&nbsp;:

> Lessy est un gestionnaire de temps basé sur des principes forts. Il est
> conçu pour être aussi **intuitif et inclusif** que possible. Son objectif est
> de redonner à ses utilisateurs et utilisatrices **leur capacité à gérer leur
> temps**, **leur confiance en eux** et **leur capacité à se remettre en
> question**. Il est développé par **une communauté accueillante et
> diversifiée**.

Cette vision est donnée en préambule [du fichier `README` de Lessy](https://github.com/marienfressinaud/lessy/blob/master/README.md)
et est immédiatement suivi d’une précision&nbsp;: toute fonctionnalité
développée, toute discussion que nous avons au sein de la communauté, toute
décision que nous prenons doit aller dans le sens de cette vision.

Voilà donc pour la mise en bouche. Je regrette souvent le manque de direction
dans les projets communautaires qui mène bien souvent à des logiciels qui font
beaucoup de choses mais qui manquent de personnalité. Avec Lessy, je veux
donner un cap au logiciel et mes engagements personnels transparaissent dans
cette vision. Lorsque j’ai commencé à développer cette application, c’était
pour me redonner la main sur le temps qui m’échappait, alors pourquoi ne pas
complètement l’assumer jusque dans l’ADN de Lessy&nbsp;?

On m’a demandé l’autre jour en quoi Lessy est-il éthique. J’étendrais la
question à en quoi Lessy correspond aujourd’hui à la vision que je viens
de donner. Et c’est vrai qu’en utilisant Lessy on voit difficilement une
application éthique, inclusive, redonnant un peu de pouvoir à ses utilisateurs
et utilisatrices&nbsp;; tout au plus une todo-list un peu originale. Et c’est
effectivement le défi que je me donne pour les mois à venir&nbsp;: faire
transparaître la vision dans l’interface. Cependant, je ne pense pas être
capable de faire cela à moi seul et c’est pourquoi je voulais rapidement me
concentrer sur la documentation communautaire. Cette documentation contient
donc désormais&nbsp;:

- [le README](https://github.com/marienfressinaud/lessy/blob/master/README.md),
  véritable porte d’entrée du projet&nbsp;;
- [un fichier de contribution](https://github.com/marienfressinaud/lessy/blob/master/CONTRIBUTING.md)
  expliquant comment participer à Lessy, j’ai pris le soin de lister une
  majorité d’activités ne nécessitant pas de connaissances en développement
  informatique&nbsp;;
- [un code de bonne conduite](https://github.com/marienfressinaud/lessy/blob/master/CODE_OF_CONDUCT.md)
  pour assurer qu’aucun comportement déplacé ne sera **jamais accepté** au sein
  de cette communauté&nbsp;;
- [une liste des contributeurs](https://github.com/marienfressinaud/lessy/blob/master/CONTRIBUTORS.md)
  pour remercier celles et ceux ayant contribué de près ou de loin au projet.

[La documentation technique](https://github.com/marienfressinaud/lessy/blob/master/docs/index.md)
a elle aussi été largement revue pour aborder plus de sujets différents.

J’espère que tous ces documents sauront attirer une population hétérogène de
contributeurs et contributrices, bien que je ne me fasse pas d’illusion&nbsp;:
il faut désormais que je me bouge pour rendre l’application réellement
attrayante.

Tout cela m’a pris beaucoup de temps à rédiger, surtout que tout a été fait en
anglais alors que ce n’est pas ma langue de prédilection. J’ai fait ce choix
volontairement en me disant qu’il serait plus facile de trouver des personnes
souhaitant contribuer en visant large qu’en visant un public uniquement
francophone. De plus, j’ai le sentiment que les Français·e·s sont de plus en
plus à l’aise avec l’anglais. Seul l’avenir me le dira&nbsp;!
