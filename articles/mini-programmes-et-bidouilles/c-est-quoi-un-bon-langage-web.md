---
title: C’est quoi un bon langage Web ?
date: 2023-02-22 09:00
---

Pour m’amuser, je me suis mis en tête d’inventer un langage Web côté serveur.
Mon objectif est d’en faire un langage pédagogique pour enseigner la programmation de sites et d’applications Web.

Jusque-là, le langage de prédilection pour cela est <abbr>PHP</abbr>.
Deux raisons principales à cela selon moi :

1. il est à la fois un langage de programmation et un mini-framework (lecture directe des entrées via les variables globales `$_GET`, `$_POST`, etc. ; sortie avec de simples `echo` ; c’est aussi un langage de templates)
2. une fois bien configuré, un site en PHP peut s’installer très facilement sur un serveur : déposez les fichiers et ouvrez le site directement dans le navigateur

PHP s’est amélioré avec le temps, mais il possède toujours de gros défauts issus de son histoire.

Quelques qualités pour un meilleur langage Web :

- très peu de concepts et de mot-clés à apprendre ;
- une seule manière de faire les choses ;
- une syntaxe claire et non ambigüe ;
- un ensemble cohérent.

Je pense m’inspirer de langages comme Python et Ruby pour le côté programmation, tout en simplifiant ; de Jinja2 pour le côté template.

**La question majeure que je me pose : à quoi ressemblerait un langage intuitif pour des personnes qui n’auraient jamais codé ?**
Dit autrement : comment appliquer des concepts d’expérience utilisateur (<abbr>UX</abbr>) à la conception d’un langage informatique ?

Je précise enfin qu’il s’agirait, pour moi, de (ré-[^1])apprendre à développer un langage de zéro.
Je cherche à tâtonner et à faire mes propres erreurs.
Et je veux m’amuser à le créer.

Mais je sais pas quand je le ferai !

[^1]: J’ai quelques souvenirs de mes cours en école d’ingé, mais tout cela est bien loin !
