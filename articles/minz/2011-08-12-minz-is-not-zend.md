Date: 2011-08-12 08:55
Title: MINZ Is Not Zend
Slug: minz-is-not-zend
Tags: Minz, php
Summary: MINZ (ou Minz) est un framework PHP que j’ai développé de 2011 à 2013. C’est probablement le projet qui m’a le plus fait progresser dans mes méthodes de développement bien que le projet reste imparfait par bien des aspects. Le code est toujours disponible [sur GitHub](https://github.com/marienfressinaud/MINZ).

Voilà voilà ☺
Ça fait un moment que je devais le faire, c’est chose faite : j’ai mis en ligne mon petit framework PHP. Pour son nom c’est évidemment un clin d’oeil au [projet GNU](https://secure.wikimedia.org/wikipedia/fr/wiki/Gnu) (pour GNU’s Not UNIX) et à [Zend Framework](http://framework.zend.com/).

Pour la petite histoire, j’ai découvert Zend cette année durant mon projet tuteuré de DUT et ça m’a pas mal inspiré. J’avais plein de projets en tête et il me fallait un framework. Trouvant que ceux existant ne s’appliquaient pas à mes petits projets, j’ai décidé de m’en coder un tout seul. C’est comme ça qu’est donc né ce framework. À vrai dire ça a été très intéressant car j’ai pu me pencher sur tout un tas de trucs que je ne connaissais pas et qui sont vraiment géniaux ! ☺

Ceci dit, MINZ est certes très léger, mais il manque encore un peu de flexibilité. J’aurai tout un tas de petites choses à ajouter (notamment étendre le bootstrap pour l’application elle-même). Cependant, il se tient déjà très bien comme ça, la preuve, mon site personnel tourne sous MINZ ;)

Je n’ai pas (encore) écrit de documentation, cependant, vu qu’il repose sur plusieurs mécanismes de Zend, si vous connaissez déjà ce dernier, l’utilisation de MINZ en sera très facile. Il repose d’ailleurs aussi sur le modèle [MVC](https://secure.wikimedia.org/wikipedia/fr/wiki/Mod%C3%A8le-Vue-Contr%C3%B4leur). De plus, la mini-application fournie avec celui-ci permet d’en comprendre très facilement le fonctionnement... en tout cas je l’espère. J’ai commenté la partie qui permet de gérer la base de données (dans la configuration et dans le modèle "User") afin que même si je n’utilise pas la BD ici, vous puissiez comprendre facilement comment l’utiliser.

Bref ! N’hésitez pas à l’utiliser, à le modifier, le sécuriser, etc. Je n’ai qu’un souhait, ne pas l’avoir fait pour moi seul. Le code est bien évidemment placé sous licence [AGPL 3](https://www.gnu.org/licenses/agpl.html) comme mes autres applications ;)
