---
title: CSS minimale pour des sites moins moches
date: 2023-03-01 08:15
---

Je suis retombé récemment sur les sites satiriques [motherfuckingwebsite.com](http://motherfuckingwebsite.com) et [bettermotherfuckingwebsite.com](http://bettermotherfuckingwebsite.com)[^1].
Ce qui m’intéresse sur ce dernier, c’est qu’il se contente de 7 lignes de <abbr>CSS</abbr> pour rendre le site beaucoup plus lisible.
En me basant dessus, je vous propose une variante légèrement améliorée :

```css
body {
    max-width: 60ch;
    margin: 3rem auto 10rem;
    padding: 0 0.5rem;
    color: #444;
    font-size: 120%;
    line-height: 1.6;
    background-color: #f9f9ff;
}

h1, h2, h3 {
    line-height: 1.2;
}

img {
    max-width: 100%;
}
```

Quelques explications vis-à-vis de mes adaptations :

- j’utilise des unités relatives (rem, ch) pour que les tailles/marges s’adaptent au contenu ;
- la taille de la police est déclarée en pourcentage pour s’adapter aux préférences du navigateur de l’utilisateur ;
- l’espace en bas du contenu est élargi car il est moins agréable de lire le texte collé au bas de l’écran ;
- le fond de l’écran est légèrement grisé pour atténuer la luminosité de l’écran (et tire vers le bleu pour que ce soit moins terne) ;
- les images sont limitées à 100% de la taille maximale du contenu afin qu’elles ne dépassent pas de l’écran.

[^1]: Notez que ces deux sites « minimalistes » ont réussi l’exploit d’intégrer Google Analytics. Sérieux ?
