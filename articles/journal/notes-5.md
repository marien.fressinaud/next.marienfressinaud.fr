---
title: Notes #5
date: 2020-02-24 15:59
blog: false
---

Je me suis installé un petit tableau blanc sur le plan de travail de ma
cuisine. Je m’en sers pour tenir ma liste de courses et lister les tâches
non-numériques que j’ai à faire. J’utilisais jusque-là plus ou moins
[Lessy](https://lessy.yuzu.ovh/) pour ça, mais j’étais gêné parce que je sentais que
ce n’était pas leur « place ». Le tableau blanc permet de mieux contextualiser
les tâches en fonction de leur nature. Je suis moins tenté de faire autre chose
lorsque je lis « laver la salle de bain » quand je suis debout, loin du PC.

---

Hier, balade à trois vélos le long de l’Isère. On a vraiment une chance énorme
de vivre en ville et pouvoir en sortir en quelques coups de pédales. Le vélo
que j’ai acheté [la semaine dernière](notes-4.html) et super léger, ça me
change du Métrovélo (le système de location de vélo de Grenoble) et me donne
encore plus de sortir avec. Mais j’ai aussi plus peur de me le faire voler…

Quand je vois qu’une candidate aux municipales veut rouvrir le centre-ville
aux voitures, je pleure.

---

Je découvre (plus ou moins) [les articles](https://jonathanlefevre.com/outils/reflexions/)
de Jonathan Lefèvre sur ses méthodes et réflexions liées à la « productivité ».
Il y a plein de petites choses que je découvre ou que je fais déjà en allant
parfois plus loin. Ça peut donner des idées, par exemple :

- [ajouter de la friction](https://jonathanlefevre.com/outils/ajouter-friction/) ;
- [éloigner son téléphone de son lieu de sommeil](https://jonathanlefevre.com/outils/eloigner-telephone-lieu-sommeil/) ;
- [désactiver les notifications](https://jonathanlefevre.com/outils/desactiver-notifications/).

Sur Twitter, il tient [un fil](https://twitter.com/Jodiroga/status/1227124371665358848)
dans lequel il republie (?) ses articles.

Sur la notion de « productivité », [il s’en explique ici](https://jonathanlefevre.com/outils/mot-productivite/).
Pour ma part, je crois que mon principal objectif aujourd’hui est d’arriver à
regagner le temps que les applications (web ou mobile) nous « volent » à
travers leur design de l’attention.

---

Je vois la fin de mon cycle de travail actuel approcher de sa fin. Je n’ai pas
l’impression d’avoir énormément avancé sur Flus, mais cela s’explique par le
fait que je suis en phase de maturation. Je souhaitais, sur février, clarifier
« là » où je voulais aller le reste de l’année : j’ai désormais une bonne
vision d’ensemble. Toutefois, en présentant mon projet à quelqu’un qui ne
connait pas du tout, je me suis rendu compte qu’il y avait encore quelques
zones d’ombres et des questions auxquelles je ne savais pas répondre.

---

J’ai bossé la semaine dernière sur un logiciel pour [Maïtané](https://www.maiwann.net/)
afin qu’elles puissent réaliser des chroniques d’activité. Il s’agit de relever
les actions et les conditions dans lesquelles une personne observée travaille.
L’observation faite en temps réel ou à partir d’une vidéo produit un graphique
et des statistiques.

Je voulais lui fournir une version fonctionnelle le plus rapidement possible
afin qu’elle puisse tester en condition réelle. Le résultat, est [Aku](https://marienfressinaud.frama.io/aku/)
([code source](https://framagit.org/marienfressinaud/aku)). Ce n’est pas encore
fini, il reste quelques fonctionnalités à développer et surtout il me faut
améliorer le graphique.

Je ferai sans doute un véritable article quand on aura une v1.

---

Suite à ma note de la semaine dernière, j’ai eu pas mal de retours de personnes
qui me lisent : merci pour vos réponses. Je ne sais pas trop ce que ça m’a
apporté, mais j’en tirerai peut-être quelque chose plus tard… ou pas 😉.
