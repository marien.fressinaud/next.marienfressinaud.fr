Date: 2011-04-01 20:08
Title: marienfressinaud.fr v2
Tags: màj
Summary: Mon site existe depuis 2010. Lors de la migration d’avril 2016, j’ai fait le grand ménage dans les articles. Je trouvais toutefois intéressant de conserver les articles liés à la vie du site pour voir comment celui-ci a grandi.

Et voilà une petite v2 du site ! ☺

Côté visiteurs, pas grand chose qui change (mise à part sans doute au niveau du blog qui est mieux intégré, avec possibilité maintenant de laisser des commentaires ;))

Par contre, désormais j’ai une petite zone "administration" qui est tout à fait sympathique et beaucoup plus agréable que de taper du texte en dur !

Tout ça m’aura pris un certain nombre d’heures, mais le jeu en valait la chandelle ☺ J’ai pu améliorer mon **semblant** de framework pour le sécuriser un peu plus. En effet j’ai récupéré ce que j’avais commencé à faire pour mon projet MyCalendar, qui ne demande pas beaucoup de sécurité puisqu’il est censé être installé uniquement en local dans un but personnel ;) Mais là vu que le site est accessible à n’importe qui, il a fallu que je m’assure de la sécurité... et je suis plutôt content ☺

Le passage de local au serveur s’est très bien passé et j’ai été plutôt bluffé d’ailleurs. Mais je suppose que ça vient du fait que je suis de plus au plus au point niveau PHP et que j’ai pensé à pas mal de choses au préalable.
Bon par contre c’est sûr, il reste encore un peu de boulot avant de pouvoir partager les sources, mais une fois que ce sera bon, je mettrai à jour la zone "projets" ;)
