Date: 2014-02-19 21:12
Title: Sortie de FreshRSS 0.7.1
Slug: sortie-de-freshrss-0-7-1
Tags: php, freshrss
Summary: FreshRSS est le premier projet conséquent que j’ai porté. Initié en novembre 2012, la version 1.0 est sortie début 2015 seulement. J’annonçais peu après que je donnais les clés à la communauté pour reprendre le code [disponible sur GitHub](https://github.com/FreshRSS/FreshRSS). J’ai toutefois aujourd’hui toujours des idées pour l’améliorer.

Un court article pour vous prévenir que FreshRSS 0.7.1 vient tout juste de sortir.

C’est un peu plus qu’une version de maintenance vu qu’il y a quelques nouveautés intéressantes comme les raccourcis pour naviguer dans la liste des catégories / flux, des performances accrues au niveau du cache et quelques corrections visuelles. Toutes les modifications sont indiquées dans le [changelog](https://github.com/FreshRSS/FreshRSS/blob/master/CHANGELOG).

Ce qu’apporte vraiment cette version, c’est une correction d’un bug de rechargement infini de la page au niveau de Persona et *J’ESPÈRE* aussi un bug de redirection infinie au niveau de PHP. Pour ce dernier bug je n’ai pas été capable de le reproduire mais j’ai amélioré le système de redirection de la page d’accueil donc ça devrait fonctionner.

Pour ce qui est du "bug" qui fait que les tables sont dupliquées : en fait ce n’en est pas un. Cela survient lorsque vous changez le préfixe des tables (typiquement en 0.6 vous n’en aviez pas et en 0.7 vous en avez ajouté un) : il est considéré alors qu’on fait une nouvelle installation et les tables ne sont donc pas migrées. Ce sera plus clair pour la 0.8 :)

Pour installer cette version, c’est simple : soit vous faites une installation toute neuve en écrasant tout, soit vous faites un `git pull` (je suppose que ceux qui ne comprennent pas ce que je dis ne sont pas concernés par cette deuxième méthode) ou alors vous pouvez faire une mise à jour :

* Supprimez tous les fichiers sauf ceux du répertoire `./data`
* [Récupérez le code](https://github.com/FreshRSS/FreshRSS/releases/tag/0.7.1) et déployez le tout comme avant
* Si vous utilisiez la version 0.7.0, vous pouvez vous permettre de supprimer le fichier `./p/i/install.php`, sinon je vous renvois sur mon article précédent pour migrer proprement.

Et comme d’habitude si vous avez une question, une suggestion ou simplement envie de m’envoyer un message pour le plaisir vous avez les commentaires, Github ou encore mon adresse mail :)
