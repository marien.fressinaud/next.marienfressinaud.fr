Date: 2012-02-11 16:52
Title: La v4 nouvelle est arrivée
Slug: la-v4-nouvelle-est-arrivee
Tags: màj
Summary: Mon site existe depuis 2010. Lors de la migration d’avril 2016, j’ai fait le grand ménage dans les articles. Je trouvais toutefois intéressant de conserver les articles liés à la vie du site pour voir comment celui-ci a grandi.

La dernière grosse mise à jour du site datait de juillet dernier. Aujourd’hui, je passe à la v4 du site, et ce n’est pas pour me déplaîre !

Au menu, de nombreuses améliorations pour faciliter la navigation sur le site, et l’administration de mon côté.

## Une nouvelle manière de me connecter

C’est une chose qui me trottait en tête depuis un moment, et c’est chose (plus ou moins) faite. L’identification sur le site passe par XMPP. Je n’ai qu’à entrer mes identifiants, le serveur se charge alors d’interroger le serveur XMPP correspondant qui dit si oui ou non les identifiants sont ok.
Trouvant la chose bien utile, et permettant de démocratiser le XMPP, j’ai décidé d’intégrer cette façon de faire nativement dans MINZ.
Mais malheureusement, vu qu’une bonne nouvelle n’arrive jamais seule, cela ne fonctionne pas comme il faut actuellement sur le serveur. Je suppose que c’est une histoire de pare-feu qui bloque le port 5222 (le port utilisé par XMPP) ☹

Du coup, poussé par ce besoin de ne pas éparpiller mes identifiants, et l’envie de découvrir d’autres choses, je me suis penché sur [OpenId](https://fr.wikipedia.org/wiki/Openid)... et finalement, ça marche plutôt bien ☺ L’identification se passera donc ainsi, mais j’ai dans l’idée d’offrir le choix. Puis ça me permettra de mettre en place le [design pattern Factory](https://fr.wikipedia.org/wiki/Fabrique_%28patron_de_conception%29) :p

## Une refonte partielle du design

C’est un de mes principaux défauts, je suis un éternel insatisfait du design. Il me faut toujours le reprendre. Celui-ci, se basant grandement sur le thème précédent voit disparaître la colonne de droite pour l’insérer dans le footer. Le header voit sa place réduite au strict minimum. Du coup, le contenu du site se retrouve vraiment mis en avant.

La flèche permettant de remonter en haut de la page reste désormais toujours affichée en bas à droite de l’écran.
À part ça, j’ai essayé d’homogénéiser toute l’interface afin de ne pas avoir une interface trop brouillone.

## Du nouveau pour le blog

Le blog se dote aussi de son lot de nouveautés. Les tags tout d’abord : afin de décrire plus précisément mes articles, j’ai toujours trouvé les tags super pratiques. J’ai eu un peu de mal à me décider sur une implémentation mais j’ai décidé de m’inspirer du Shaarli de SebSauvage au final. Un nuage de tags a été intégré en bas de la page, et <del>le gros du travail va être désormais de mettre à jour les différents articles déjà publiés pour les tagger.</del> -> c’est fait ☺

De plus, désormais j’aurai la possibilité d’enregistrer mes articles à venir sous forme de brouillons. Très pratique si je veux revenir sur mes articles tout en pouvant les prévisualiser, sans avoir à passer par un autre éditeur.

Enfin, depuis un petit moment j’ai enlevé les commentaires sur le blog. Le choix s’expliquant par le fait que je n’ai aucun commentaire réellement "intéressant" et que je me fais polluer par le spam (oui je pourrais ajouter un captcha, mais je n’aime pas ce système). Sur le coup je l’ai fait assez "salement", mais désormais, j’ai une option codée à peu près correctement et je pourrai les (dés)activer comme bon me semble. Et pour la peine, ceux-ci sont réouverts (d’autant plus que la suppression du spam n’aura jamais été aussi simple que maintenant ☺)

## Des nouveautés en vrac

L’habituelle correction de bugs est bien entendue au rendez-vous. Quelques ajouts par-ci par-là au niveau de l’administration ainsi que quelques révisions dans la manière de coder me permettent d’avoir un code plus propre et un site plus stable ☺

## En conclusion

J’ai hésité à estampiller cette version de "4", mais vu que la mécanique du site ressent quand même un gros changement et que j’ai ajouté à peu près toutes les idées qui me restaient en tête, je pense que cette version le mérite ☺
