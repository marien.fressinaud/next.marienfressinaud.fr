Date: 2017-07-23 21:00
Title: Project Zero 0.3
Slug: project-zero-0-3
Tags:
Summary: La nouvelle version de Project Zero est arrivée à maturité avec de nouveaux indicateurs, une page de statistiques et une simplification générale du système.

Un mois et demi après la 0.2, je suis très content d’annoncer la sortie de la
version 0.3 de Project Zero. Pour rappel, il s’agit d’un gestionnaire de
projets et de tâches avec un fort parti pris sur la façon de gérer tout cela.

Le code source se trouve [sur GitHub](https://github.com/marienfressinaud/project-zero),
sous licence MIT et vous pouvez l’utiliser sur [zero.marienfressinaud.fr](https://zero.marienfressinaud.fr),
en attendant de migrer vers un nouveau (sous-)domaine que j’ai (enfin !)
acheté.

## Les nouveautés

Lorsque j’ai annoncé le mois dernier ce que je comptais inclure dans la
prochaine version, j’avais prévu toute une liste de choses à faire :
« meilleure gestion des petits écrans, emails de rappel, joli logo,
amélioration de l’ergonomie et refonte du design, éventuellement
ludification ». De tout ça, je n’ai quasiment rien retenu pour me concentrer
sur ce qui comptait vraiment dans mon usage, l’idée étant que je l’utilise
réellement au quotidien.

Premièrement, le « responsive design » était quasiment primordial vu que
j’utilise beaucoup Project Zero sur tablette, voire sur téléphone. Ce n’est pas
encore parfait, surtout sur mobile, mais tout est fonctionnel.

Ensuite, j’avais absolument besoin de lier des tâches aux projets pour
m’assurer qu’ils avancent correctement. En effet, jusqu’à maintenant ma liste
de projets servait surtout de déco et beaucoup d’entre eux dépassaient leur
date de fin. Désormais, avec les tâches associées ainsi que les deux
indicateurs qui viennent avec, il est désormais beaucoup plus facile de savoir
ce qui avance ou non. Les indicateurs sont de deux types :

- le nombre de tâches terminées vs. le nombre de tâches associées, cet
  indicateur devient orange s’il n’y a aucune tâche à faire et est rouge s’il
  n’y a aucune tâche associée ;
- une barre de progression montrant où l’on en est (càd. le jour courant) par
  rapport à l’échéance de fin.

Ces deux indicateurs se sont révélés indispensables pour mon usage.

![Dashboard de Project Zero 0.3](images/projectzero/2017-07-23-0.3-dashboard.png)

Une fonctionnalité de la version 0.3 s’est en revanche montrée moins utile. En
effet, les tâches non terminées le jour même se retrouvaient dans une « pending
list » qui n’était pas facilement accessible. De ce fait, je perdais de vue bon
nombre des tâches qui se trouvaient donc non résolues. J’ai décidé de renvoyer
ces tâches dans le « backlog » dans le but de toujours démarrer une journée
avec une liste de tâches vide. J’en ai profité pour rendre les tâches du
« backlog » directement plannifiable ce qui a rendu le bouton « *What will you
work on today ?* » inutile. C‘est donc un écran qui a sauté.

En résumé, il n’y a plus que deux listes de tâches : la liste du jour même et
le « backlog ». Chaque début de journée, vous pouvez aller piocher dans
celui-ci les tâches que vous pensez pouvoir réaliser le jour même.

Pour terminer, une fonctionnalité pas tellement essentielle mais que je
trouvais sympa : une page de statistiques permettant de retracer sur les 15
derniers jours le nombre de tâches créées comparé au nombre de tâches
terminées. Cela permet de me rassurer sur le fait que j’avance dans ce que j’ai
à faire.

![Dashboard de Project Zero 0.3](images/projectzero/2017-07-23-0.3-statistics.png)

Pour les plus vigilants, vous noterez que le style est très proche de celui des
graphiques GitHub (ceux de l’onglet « Traffic »).

Avec cette nouvelle version, j’ai désormais un outil pleinement fonctionnel
pour mon usage quotidien. Je vais désormais sans doute mettre un (petit) coup
de frein au développement du projet pour préparer un gros morceau :
l’amélioration globale de l’expérience utilisateur et la mise en place d’une
charte graphique pour le projet. Je ne sais pas encore quand je pourrai faire
cela et il n’est pas impossible que je développe deux trois fonctionnalités
autres, mais ce n’est pas dans mes priorités immédiates.
