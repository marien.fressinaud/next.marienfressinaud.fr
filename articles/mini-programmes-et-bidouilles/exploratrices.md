---
title: Exploratrices, un prototype de jeu vidéo
date: 2023-01-26 11:17
update: 2023-03-30 19:10
---

J’ai créé un prototype de jeu vidéo.

Il s’agit d’un jeu narratif qui se joue avec des commandes textuelles.
C’est-à-dire que c’est un jeu qui se « lit » et il vous faut écrire des commandes pour jouer.
Ce gameplay est inspiré des « [<i lang="en">Multi-user dungeon</i>](https://fr.wikipedia.org/wiki/Multi-user_dungeon) » (aussi appelés MUD).
J’étais curieux d’en créer un dans un navigateur.

Il se nomme « Exploratrices » et vous y incarnez une Exploratrice qui arrive dans le village de Sylvaluna.
Le but est simplement de vous balader pour en apprendre plus sur le village en observant ses habitantes.

**<del>Vous pouvez y jouer sur exploratrices.yuzu.ovh.</del>**

<del>Information importante : ce prototype ne restera en ligne qu’au maximum pendant 2 mois.</del>

Mise à jour : le prototype n’est plus en ligne !

Il est extrêmement court.  
Il est extrêmement simple.  
Il est extrêmement rudimentaire.  
Mais il est terminé, et c’est le plus important.

---

Je ne compte pas continuer ce prototype.
Taper des commandes n’a pas beaucoup de sens en termes de gameplay dans un jeu dont ce n’est pas le thème.
C’est même assez vite chiant dans un navigateur Web.
C’était ce que je voulais tester et je sais désormais que je veux une expérience de jeu différente.

Cela étant dit, les quelques lignes de code que j’ai pu écrire pour ce projet m’ont donné plein de perspectives, y compris du multi-joueurs.
Je ne sais pas si je passerai beaucoup de temps là-dessus cette année, mais je sais que ce ne serait pas si compliqué de faire quelque chose de chouette.

Et puis je me suis attaché à l’univers, j’aimerais l’explorer un peu plus à l’occasion.

---

Si vous voulez en apprendre plus sur le projet, j’ai tout détaillé dans le fichier README du projet.
Vous le trouverez ici : [framagit.org/marienfressinaud/exploratrices](https://framagit.org/marienfressinaud/exploratrices/-/blob/main/README.md)

J’y parle des mécaniques de jeu, de la technique et même du processus d’écriture avec… ChatGPT.
