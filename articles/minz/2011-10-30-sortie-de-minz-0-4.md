Date: 2011-10-30 16:31
Title: Sortie de Minz 0.4
Slug: sortie-de-minz-0-4
Tags: Minz, php
Summary: MINZ (ou Minz) est un framework PHP que j’ai développé de 2011 à 2013. C’est probablement le projet qui m’a le plus fait progresser dans mes méthodes de développement bien que le projet reste imparfait par bien des aspects. Le code est toujours disponible [sur GitHub](https://github.com/marienfressinaud/MINZ).

Suite à la version 0.3 du mois précédent j’avais quelques améliorations à apporter à mon framework.

Tout d’abord, le routage des urls internes au site avait été un gros morceau, mais très incomplet à mes yeux. Aussi j’avais vraiment besoin d’améliorer ceci. C’est désormais chose faite : quelques bugs ont été corrigés et surtout, le routage d’urls dynamiques est désormais possible ! ☺
Ainsi, si on définit le code suivant (tiré de mon appli de galeries photos) dans la table de routage :

```php
array(
    'route'       => '/see/galerie/*/photo/*',
    'controller'  => 'index',
    'action'      => 'see',
    'params'      => array('gal', 'photo')
),
```

l’url `http://test.exemple/see/galerie/2/photo/42` (par exemple) sera routée vers le contrôleur `index`, l’action `see`, et avec les paramètres `$_GET['gal']=2` et `$_GET['photo']=42`. C’était vraiment un gros morceau, pas facile à mettre en place, mais je suis content de l’avoir fait !

Un autre morceau que je voulais coder était une classe de configuration, au lieu de passer par des constantes globales. J’en ai profité pour revoir le fichier de configuration qui sera désormais un fichier `.ini` (même si le fichier de constantes est pris en charge). Les paramètres `environment` et `use_url_rewriting` ont été ajoutés et sont indispensables. `environment` permet de définir un niveau de verbosité (les logs et les erreurs seront plus ou moins bavards). `use_url_rewriting` quant à lui sert à dire si on souhaite utiliser l’url_rewriting ou non. D’ailleurs il n’existe pas de fonction php servant à savoir si ce module est activé ou non, et c’est bien dommage ☹

Je viens de citer les logs. En effet, je trouvais ça intéressant de les intégrer au cœur du framework afin de débugguer une application. Pour le moment c’est perfectible, mais je suis content du résultat.

J’ai ajouté une classe `Url` permettant de créer dynamiquement les urls internes au site. En effet, à cause de l’url rewriting et de mon système de routage, il pouvait y avoir des problèmes si jamais le serveur ne permet pas de réécrire les url. Mais maintenant c’est de l’histoire ancienne ! ☺ La classe `Url`, à travers sa méthode statique `display()` va s’occuper de voir si on utilise l’url rewriting (à travers le paramètre de configuration déjà nommé plus haut). Si oui, il va chercher dans la table de routage si l’url que l’on souhaite écrire possède une route et si oui, la construit pour l’affichage. Si non, il construit l’url de façon plus basique (du type `http://test.exemple/?c=blog&a=voir&id=42` ). Au final ça rend le framework très puissant car il gère tout seul l’url rewriting, et on n’a plus à se soucier de savoir si le module est activé sur le serveur ☺
Et pour construire l’url, la ligne suivante suffit :

```php
echo $this->url->display(array(
    'c' => 'blog',
    'a' => 'see',
    'params' => array('id' => 42)
));
```

... bon d’accord, ça peut paraître lourd, mais au vu des bénéfices, je pense que ça vaut largement le coup ;)

Enfin, dernière amélioration importante, la gestion des pages d’erreurs a été améliorée. Avant, je me contentais d’afficher une page blanche avec les messages d’erreurs. Désormais, les erreurs peuvent être intégrées au design du site. Comme dit précédemment, les erreurs affichées sont gérées grâce au niveau de verbosité défini par le paramètre de configuration `environment`.

Et sinon j’ai corrigé plusieurs petits bugs, fait quelques retouches, bricolé par-ci par-là. Bref, cette version 0.4 n’est plus vraiment compatible avec les versions précédentes, bien qu’une migration puisse sans doute se faire sans trop de mal. Je l’ai bien fait pour mon appli de galeries photos ;)
Le plus compliqué sera de migrer mon site sur cette version, mais ça vaudra surement le coup ! J’ai vraiment hâte d’intégrer les urls dynamiques et ma galerie photos... malheureusement ma connexion Internet ne me permet pas de faire du ftp ni sftp. Bref, ça va encore devoir attendre ;)

Pour la prochaine version, j’espère pouvoir intégrer une classe pour gérer l’internationalisation du site (ce qui va demander pas mal de boulot), corriger des bugs (en espérant que cela ne soit pas nécessaire :p), améliorer tout ce qui est `Model` et `DAO` et surtout, proposer de la documentation conséquente ! J’ai déjà commencé à réaliser un document, mais ça demande pas mal de boulot pour synthétiser, faire des exemples, des schémas, etc. Puis avec cette nouvelle version, il y a quelques bidules à remanier...
