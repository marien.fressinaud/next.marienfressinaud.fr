Date: 2017-11-26 17:00
Title: Lessy Apus
Slug: lessy-apus
Tags:
Summary: Je sors aujourd’hui Lessy Apus. Au programme, une toute nouvelle interface refaite de fond en comble et un guide édictant un certain nombre de règles de design.

Après un mois de travail il est enfin temps pour moi de sortir cette nouvelle
version&nbsp;: **Lessy Apus**&nbsp;!

![Dashboard de Lessy Apus](images/lessy/2017-11-26-apus-dashboard.png)

Je dois bien avouer que je ne savais pas trop où je mettais les pieds en
démarrant cette refonte complète. J’ai failli me contenter de publier un guide
de design et de faire la migration des différents composants au fil de l’eau.
Mais un problème s’est rapidement posé. Souhaitant que le guide reste au plus
proche du code, celui-ci a été développé directement au sein de Lessy. De ce
fait, modifier le guide signifiait aussi modifier le reste de l’application. De
fil en aiguille, je me suis ainsi trouvé à tout redévelopper.

Travail de longue haleine s’il en est, la refonte a consisté à&nbsp;:

- créer un guide accessible sur [lessy.yuzu.ovh/design](https://lessy.yuzu.ovh/design)&nbsp;;
- réécrire (tous) les composants de base pour qu’ils correspondent aux règles
  édictées dans le guide&nbsp;;
- réorganiser le code pour le rendre plus modulaire et plus maintenable&nbsp;;
- mettre en place des tests automatisés pour l’interface (très basiques pour le
  moment)&nbsp;;
- revoir un certain nombre d’éléments ergonomiques (des boutons plus
  explicites, une meilleure gestion des erreurs, des comportements moins
  surprenants, etc.)&nbsp;;
- corriger de nouveaux bugs ou identifier des problèmes de performance (sinon
  c’est pas marrant)&nbsp;;
- repenser la page d’accueil pour la rendre plus accueillante et présenter le
  projet.

Il existe toujours des éléments dont je ne suis pas entièrement satisfait, mais
l’essentiel est là, le reste suivra durant les prochains jours. Je suis en tout
cas extrêmement satisfait d’avoir désormais un guide de design qui rend ce
dernier plus facile à faire évoluer et rend plus difficile de sortir des clous.

J’ai profité de sortir cette version pour ouvrir [un compte sur Liberapay](https://liberapay.com/Lessy)
afin d’essayer de rembourser au moins les frais de serveur + nom de domaine
puis éventuellement pouvoir plus tard me dégager un peu plus de temps pour le
projet. Il y a mine de rien plus de 250 comptes ouverts sur lessy.yuzu.ovh dont
quelques 65 activés, sachant que je suis très loin encore d’avoir mis en place
tous les outils pour assurer un service satisfaisant. De la supervision&nbsp;?
Non. Des conditions générales d’utilisation&nbsp;? Ah ah&nbsp;! Des
backups&nbsp;? Ah ça oui&nbsp;! Et ils fonctionnent&nbsp;? Heuuu… Vous voyez
l’idée donc.

Le service est toujours disponible sur [lessy.yuzu.ovh](https://lessy.yuzu.ovh), il reste
ouvert et gratuit pour le moment, mais sans doute plus pour très longtemps
donc. Le code reste [hébergé sur GitHub](https://github.com/marienfressinaud/lessy)
et toujours (et à jamais) [sous licence libre](https://github.com/marienfressinaud/lessy/blob/master/LICENSE).

Pour la prochaine version ([Aquarius](https://github.com/marienfressinaud/lessy/projects/4)),
je reviendrai à du développement dans le dur en améliorant le lien entre les
tâches et les projets.
