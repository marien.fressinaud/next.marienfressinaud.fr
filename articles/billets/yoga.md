---
title: YOGA Image Optimizer
date: 2022-02-02 10:00
---

J’ai découvert il y a quelque temps un utilitaire pour optimiser ses images en local : [YOGA Image Optimizer](https://yoga.flozz.org/) (via [LinuxFr](https://linuxfr.org/news/yoga-image-optimizer-v1-1-resultats-des-travaux-de-l-ete)).

Je sais qu’il existe des tas d’applis qui font ça en ligne et d’autres qui le font en ligne de commande ; je suis juste content d’avoir une interface simple pour le faire en quelques clics sur mon ordi. J’en ai profité pour filer 5 balles au développeur pour le remercier de son travail (à noter que le développement semble actuellement être en pause).

J’ai passé les quelques images du site à la moulinette, ainsi que celles du [carnet de Flus](https://flus.fr/carnet/). J’estime avoir réduit en moyenne de 25 % le poids des images.

![Capture d’écran de YOGA listant les fichiers du carnet de Flus](images/yoga.png)