---
title: Pour bâtir un morceau de Web
date: 2022-02-07 21:40
tags: featured
---

Je me souviens très bien de la toute première page Web que j’ai écrite[^1]. Elle ressemblait à ça :

[^1]: Je m’en souviens tellement bien que je serais prêt à parier que mon voisin de bureau était en train de jouer à Dofus à ma droite tandis que je découvrais le Site du Zéro. Ça n’a pas beaucoup d’intérêt de se souvenir de ça, mais ça reste un jour important pour moi :)

![Une page Web blanche affichant « Bonjour tout le monde ! »](images/hello-world.png)

C’était incroyable : il m’avait suffi d’écrire **une et une seule ligne de texte** dans un fichier texte pour qu’elle puisse s’afficher dans mon navigateur[^2]. Je venais d’apprendre la leçon la plus importante de ma carrière : le Web, ce n’est jamais que de bêtes fichiers textes.

[^2]: Probablement Firefox 3.0 à l’époque, ça devait être en 2008 ou 2009.

J’aimerais faire passer ce message aux personnes qui voudraient apprendre à faire du Web — et même aux plus expérimentées — : **ce n’est pas plus compliqué que ça.** Vous vous sentez peut-être intimidé‧e par la complexité des outils qui permettent de faire du Web « moderne[^3] » ; la vérité, c’est que vous avez seulement besoin d’un éditeur de texte et d’un navigateur Web.

[^3]: Oubliez ce terme, il est généralement utilisé par des personnes qui aiment se compliquer la vie avec beaucoup trop d’outils. Il n’y a pas plus moderne qu’un site qui fonctionne.

Pour débuter, vous n’avez pas besoin de Webpack ou autre outil de <i lang="en">build</i>. Vous n’avez pas besoin non plus de framework React/Angular/Vue (surtout pas !) En fait, vous n’avez même pas besoin de JavaScript.

**Pour bâtir un morceau de Web à vous, vous avez seulement besoin d’apprendre à écrire du <abbr>HTML</abbr> et — un tout petit peu — de <abbr>CSS</abbr>.**

Ouvrez un éditeur de texte (Notepad sur Windows, ou gedit[^4] sur Linux par exemple), collez-y le code suivant :

[^4]: On en dira ce qu’on veut, mais j’ai codé avec gedit pendant plusieurs années et il me convenait _très_ bien. Une autre leçon tiens : ne vous souciez pas trop de ce que les autres pensent de vos outils. Les guerres de chapelles, c’est fatigant.

```html
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Mon site Web</title>
    </head>

    <body>
        <p>Vous pouvez modifier le fichier pour commencer à apprendre <abbr>HTML</abbr></p>
    </body>
</html>
```

Enregistrez ça dans un fichier nommé `test.html`, n’importe où sur votre <abbr>PC</abbr>, puis cliquez dessus pour l’ouvrir dans votre navigateur. Voilà, vous avez devant vous votre première page Web[^5] !

[^5]: Bonus : elle est valide <abbr>HTML5</abbr>, accessible, extrêmement rapide à charger et s’affiche correctement sur mobile ! On fera difficilement plus moderne.

Pour la suite, il existe des cours en ligne. La référence semble toujours[^6] être le cours « [Apprenez à créer votre site web avec <abbr>HTML5</abbr> et <abbr>CSS3</abbr>](https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3) » de Mathieu Nebra sur OpenClassrooms (ex Site du Zéro pour les nostalgiques).

[^6]: En recherchant un peu, j’ai été surpris qu’il n’existe pas tant de ressources supplémentaires. Les quelques cours que j’ai trouvés me semblent moins bons que celui d’OpenClassrooms. C’est un peu dommage si vous voulez mon avis.

**Un conseil avant tout : créez-vous un site personnel pour expérimenter des choses par vous-même.** C’est pas grave s’il ne marche qu’à moitié, c’est pas grave s’il est moche. Ne vous souciez pas trop non plus — pour le moment — d’éventuelles « bonnes pratiques ». Allez-y tranquillement.

Si vous êtes plus expérimenté‧es, je vous invite à considérer un outillage minimaliste lorsque vous débuterez votre prochain projet. Réfléchissez à comment vous pourriez limiter votre dépendance à des outils externes. Le gain offert par ces outils ne vaut pas toujours le temps passé à les configurer et tenir à jour. Limiter les dépendances, c’est aussi faciliter l’accès à votre projet à d’autres personnes. Est-ce que vous avez _vraiment_ besoin de ce framework frontend ? Et rappelez-vous que, le Web, c’est du <abbr>HTML</abbr> avant d’être du JavaScript.
