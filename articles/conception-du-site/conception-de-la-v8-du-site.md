---
title: Conception de la v8 du site
date: 2019-03-01 13:20
---

Au fil des années, j’ai pris le temps d’annoncer les changements importants du
site, je ne vais donc pas non plus déroger à la règle cette fois-ci. Cette
huitième version (quand même) arrive presque deux ans après la dernière. C’est
avant tout pour moi un retour au « fait-maison » qui était la base lorsque j’ai
démarré. J’avais été lassé de maintenir des outils souvent complexes ; [la
cinquième version](mise-a-jour-du-site-la-v5-est-la-o.html) introduisait
notamment une idée un peu fumeuse consistant à agréger les éléments que je
partageais via 3 outils que j’avais moi-même développé…

En revenant à un outil plus simple (je vous renvoie à [ma série d’articles](serie/le-boop-blog.html)),
je reviens aussi à une idée de l’informatique qui me plaît : maîtriser (ou au
moins comprendre) les outils que j’utilise.

Cet article, donc, revient sur les grandes lignes directrices que j’ai
souhaitées pour cette énième version de mon site. Notez que le contenu n’est
pas terminé, il manque notamment le manifeste qu’il me reste à rédiger et la
liste des projets que je souhaite mettre en avant. J’envisage aussi une page
« à propos » mais je ne suis pas certain de son intérêt.

## Une approche pédagogique et ouverte

Si j’ai toujours tenté d’adopter une démarche pédagogique, je crois que je
commence à avoir une certaine maturité sur le sujet. La série d’articles dans
laquelle s’inscrit celui-ci en est la preuve la plus évidente puisque j’y
explique toute la démarche qui se cache derrière ce site. Un autre exemple que
j’aime bien est celui de la partie « Traitement des données » de ma page
« [Mentions légales](mentions-legales.html) » dans laquelle j’explique que
malgré le fait que je ne collecte pas de données… et bien j’en collecte quand
même ; j’en profite pour expliquer la raison et les circonstances.

Le côté ouverture est quant à lui illustré par les liens qui renvoient à chaque
fois vers les articles qui décrivent la raison de tel ou tel paragraphe, ou
pour compléter en informations. J’ai aussi insisté sur les liens renvoyant au
code source. J’envisage, pour aller plus loin, d’ajouter pour chaque page des
liens discrets vers les fichiers sources correspondants.

L’une des conséquences de cette approche est que cela rend le site plus
verbeux. C’est voulu. Celleux qui me connaissent savent que j’écris facilement
de gros pavés et cela fait partie intégrante de qui je suis, j’assume. Pour
équilibrer cela, j’ai appliqué une série de bonnes pratiques de typographie
pour que le contenu reste agréable à lire, notamment le principe du « [_triangle
équilatéral du paragraphe parfait_](https://css-tricks.com/equilateral-triangle-perfect-paragraph/) »
(c’est-à-dire équilibrer la taille de la police d’écriture avec la largeur et
l’espacement des lignes). Le choix de couleurs qui contrastent bien entre elles
a pris un peu de temps, mais [l’outil d’accessibilité intégré à Firefox](https://developer.mozilla.org/fr/docs/Outils/Inspecteur_accessibilite)
a été d’une aide précieuse.

![Capture d’écran qui montre que le ratio de contraste du titre de cet article avec l’arrière-plan est de 14,32](images/site/v8-contraste-titre.png)

Le but de tout cela est aussi d’apposer une « patte » particulière sur mon
site, pour faire en sorte que la forme du blog épouse le fond de ce que je veux
promouvoir.

## Navigation restreinte

Deux choses m’agacent souvent dans la navigation des sites :

- trop de liens différents donnés sans aucun contexte (pourquoi cliquerais-je
  sur tel ou tel lien ?) ;
- et le menu « hamburger » (du moins sur grand écran), celui caché derrière une
  icône représentée par trois barres horizontales.

Pour cette nouvelle version du site, la navigation se passe en majorité « dans
le texte ». C’est-à-dire que la majorité des liens vers les autres pages sont
(ou seront) contextualisés au sein du texte de la page d’accueil. Dans
l’en-tête du site, seulement deux liens : l’un pour revenir à l’accueil,
l’autre pour accéder au blog (qui reste le « lieu de vie » du site). Cette
économie de navigation m’évite en plus le fameux menu « hamburger ».

Conscient que ce type de navigation est là encore verbeux, les différents liens
sont accessibles depuis le pied de page pour avoir une vue d’ensemble du site.

Le dernier élément de navigation présent sur le site est le bouton qui apparaît
en bas de la page une fois que vous l’avez faite défiler un peu et qui permet
de remonter en haut de la page. Pour le créer, je me suis basé sur un article
de [<i lang="en">Signal v. Noise</i>](https://m.signalvnoise.com/how-to-back-to-top-button-without-scroll-events/).
J’ai dû légérement adapter le code qui s’appuie sur leur cadriciel JavaScript,
ce qui donne [un petit script d’une dizaine de lignes](https://framagit.org/marienfressinaud/marienfressinaud.fr/blob/master/static/scripts/back-to-top.js).
Le [code <abbr>CSS</abbr>](https://framagit.org/marienfressinaud/marienfressinaud.fr/blob/master/static/style/herisson.css#L213-254)
quant à lui est un peu plus long, mais principalement pour l’apparence.

## Un thème moins bleu

Le thème de ce site a toujours été bleu, voire très bleu. Chose révolue donc
puisque je suis parti sur du rouge brique (il y aura bien quelqu’un·e pour me
dire que c’est orange, non ? /private-jock). Pourquoi ce rouge ? Pas de raison
particulière pour le coup, ça ne transmet pas de message particulier. On pourra
toujours disserter sur la signification des couleurs qui irait, dans le cas
présent, à l’encontre de ce que je veux transmettre ; mon pari est que ça ne
joue qu’à la marge et qu’on peut bien choisir un peu les couleurs que l’on veut
tant qu’elles nous plaisent ! Si quelqu’un vient argumenter sur le fait qu’il
trouve le thème agressif, je reconsidèrerai toutefois volontier mon sentiment.

La « charte » se décline ainsi :

- la couleur principale (<code class="color-primary">#2a2230</code>), utilisée
  principalement pour le texte, ainsi qu’une variante légèrement plus clair (<code class="color-primary-muted">#665e6c</code>) ;
- une couleur secondaire (<code class="color-secondary">#c02b43</code>) pour réhausser le ton du site ;
- une couleur spécifique pour les liens (<code class="color-link">#a6253a</code>), proche de la couleur
  secondaire ;
- et une couleur de contraste (<code class="color-contrast">#f8f6ff</code>), utilisée notamment en arrière-plan.

La règle est de ne jamais superposer deux couleurs différentes, à part la
couleur de contraste qui est prévue pour cela (son ratio avec les autres
couleurs est suffisant pour assurer une bonne lisibilité).

## Mobile avant tout

J’ai pensé le site pour fonctionner sur mobile. Même si je n’utilise plus que
très peu le téléphone pour aller sur Internet, il est toujours plus agréable de
naviguer sur les sites prévus pour (c’est quand même l’une des promesses des
technologies web, de pouvoir être accessibles quel que soit le périphérique).

Pour cela j’ai utilisé le mode « mobile » de Firefox (le raccourci pour
l’utiliser est `ctrl+maj+M`). J’ai commencé par faire en sorte de supporter la
largeur d’écran la plus petite que je souhaitais pouvoir gérer (360 pixels),
puis j’ai ajouté quelques points de « rupture » (c’est-à-dire des largeurs
d’écran pour lesquelles le style doit changer). Par exemple, pour les blocs des
séries sur la page de blog, les blocs sont affichés en colonne par défaut :

```css
.blog-series {
    display: flex;
    flex-direction: column;
}
.blog-serie {
    margin-bottom: 1rem;
}
```

Ce qui donne :

![Capture d’écran de la page principale du blog où l’on voit les blocs des séries en colonne](images/site/v8-blog-petit-ecran.png)

Mais à partir d’une largeur de `40rem` (qui correspond à 640 pixels dans ce
cas), on les affiche en lignes avec retour à la ligne autorisée.

```css
@media(min-width: 40rem) {
    .blog-series {
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
    }
    .blog-serie {
        width: 30%;
        margin-right: .5%;
        margin-left: .5%;
    }
}
```

Ce qui donne donc :

![Capture d’écran de la page principale du blog où l’on voit les blocs des séries qui sont passés en lignes](images/site/v8-blog-grand-ecran.png)

## Mise en production imminente

Ayant suffisamment repoussé la mise en production jusque-là, j'envisage de le
faire dès la semaine prochaine même si je n’ai pas tout à fait fini le contenu
du site. Tout viendra en temps et en heure. J’ai pu tester que l’importation
des articles de l’ancien site se faisait sans soucis. À part quelques flux
<abbr>RSS</abbr>, les liens ne devraient donc pas se casser.

Vous pouvez donc supprimer ce site de votre agrégateur de flux <abbr>RSS</abbr>
et repasser sur celui du [site initial](https://marienfressinaud.fr/), tous les
articles y seront transférés.
