---
title: Simplifier la rédaction d’articles dans Boop!
date: 2018-12-11 22:00
---

Fin octobre j’inaugurais la refonte de mon site web personnel en promettant
d’expliquer pas à pas les étapes qui mèneraient à une nouvelle version de
celui-ci. Le mois de novembre a cependant défilé bien plus vite que je ne le
pensais. Mon chômage ayant débuté début décembre, je peux enfin replacer mes
priorités sur les projets qui me tiennent à cœur. Alors reprenons.

## Simplifier le balisage avec <span lang="en">Markdown</span>

La première chose qui m’a émerveillé lorsque j’ai appris à faire du web il y a
quelques années, c’est qu’il suffisait d’écrire du texte dans un fichier et de
l’ouvrir dans un navigateur pour avoir un site fonctionnel. Mais pour rendre ce
site Internet lisible par les navigateurs, il est aussi nécessaire de
structurer vos fichiers [au format <abbr>HTML</abbr>](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language).
En soi rien de bien compliqué, mais cela peut vite devenir lourd. Un
paragraphe, par exemple, doit être entouré de « balises » `<p>` :

```html
<p>Mon paragraphe.</p>
```

Une liste quant à elle se définit de cette manière :

```html
<ul>
    <li>Un élément de la liste</li>
    <li>Un autre élément</li>
    <li>Et un troisième</li>
</ul>
```

Personnellement, j’ai autre chose à faire que de toujours penser à définir
correctement mes balises. C’est là que [le <span lang="en">Markdown</span>](https://fr.wikipedia.org/wiki/Markdown)
rentre en jeu. Il s’agit aussi d’un langage de balisage, mais celui-ci se veut
plus léger et naturel pour un humain. Mes deux exemples précédents deviennent
ainsi :

```markdown
Mon paragraphe.
```

Il n’y a aucun balisage, c’est normal. Et pour la liste :

```markdown
- Un élément de la liste
- Un autre élément
- Et un troisième
```

De simples tirets, c’est beaucoup plus simple, non ? Ce fut donc la première
amélioration que j’ai apporté à [<span lang="en">Boop!</span>](https://framagit.org/marienfressinaud/boop),
mon générateur de sites statiques. Pour cela, il me suffit de tester
l’extension d’un fichier, si celui-ci se termine par `.md`, je passe le contenu
du fichier au parseur <span lang="en">Markdown</span> pour qu’il le transforme
en <abbr>HTML</abbr>, puis j’écris enfin le résultat dans un nouveau fichier
([voir le <span lang="en">commit</span> introduisant le changement](https://framagit.org/marienfressinaud/boop/commit/a069ecbce2fb89107064679f2a4f27fef15143da#b8c037ff8ab16c431251e16173af8e9ec6b1cbcf)).

Ce fut aussi l’occasion de déroger à ma règle non-officielle de ne reposer sur
aucun code extérieur : j’avais mieux à faire que de réécrire un parseur complet
de <span lang="en">Markdown</span> depuis zéro. Je me base donc sur le paquet
[<span lang="en">Markdown</span>](https://pypi.org/project/Markdown/),
disponible via la commande `pip`.

## <span lang="en">Boopsy</span>, mon système de templating

J’avais ensuite un autre soucis. En effet, je souhaitais que tous les articles
partagent la même structure <abbr>HTML</abbr> et notamment :

- le même fichier de style <abbr>CSS</abbr> ;
- le titre de l’article dans la balise `<title />` ainsi que dans une balise
  `<h1 />` ;
- l’affichage de la date de publication de l’article ;
- des liens pour revenir à l’accueil en haut et en bas de page.

Pour cela j’ai pas mal tergiversé sur la meilleure manière de faire afin de
répondre du mieux possible à mon besoin tout en limitant le plus possible le
code à écrire. J’aurais pu me baser sur un système de <span lang="en">*templating*</span>
déjà existant tel que [<span lang="en">Jinja2</span>](http://jinja.pocoo.org/),
mais j’avais envie de créer quelque chose de plus simple.

Après avoir longuement lu l’article de Ned Batchelder, « [<span lang="en">A
template engine</span>](http://aosabook.org/en/500L/a-template-engine.html) »
sur [<abbr title="The Architecture of Open Source Applications" lang="en">AOSA</span>](http://aosabook.org/),
et avoir joué un petit peu de mon côté, j’ai sorti un système de
<span lang="en">*template*</span> (nommé <span lang="en">Boopsy</span>) très
simplifié par rapport à l’original : en à peine 60 lignes de code, j’avais déjà
un système fonctionnel ([voir le <span lang="en">commit</span> introduisant le
changement](https://framagit.org/marienfressinaud/boop/commit/1d4ab7d18460b0cff72f21cb30102f3999a20fea#b8c037ff8ab16c431251e16173af8e9ec6b1cbcf)).

Ce système est évidemment largement améliorable mais constitue une bonne
première étape qui fait exactement ce dont j’ai besoin.

## Encore un petit effort

Il va me rester une dernière petite étape à terminer avant de commencer à
réfléchir au contenu du site : la génération du flux <abbr>RSS</abbr> des
articles pour permettre de suivre ce blog facilement. Le reste des
fonctionnalités que je pourrais envisager ensuite sont plutôt de l’ordre du
confort et pourront attendre.

Je devrais en principe mettre moins de temps cette fois-ci pour donner des
nouvelles !
