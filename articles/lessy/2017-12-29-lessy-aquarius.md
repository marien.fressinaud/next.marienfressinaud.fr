Date: 2017-12-29 12:15
Title: Nouvelle version de Lessy : Aquarius
Slug: lessy-aquarius
Tags:
Summary: Une nouvelle version de Lessy sort aujourd’hui : Aquarius. Cette version apporte principalement l’amélioration de l’intégration des tâches avec les projets.

À peine plus d’un an après l’écriture des premières lignes de code de
l’application, je sors la septième version de Lessy. Nom de code : Aquarius.

Pour rappel, Lessy est un gestionnaire de temps destiné à vous aider à mieux
vous organiser en associant à vos tâches des indicateurs clairs sur ce que vous
avez de plus urgent à réaliser (ou abandonner) dans l’immédiat. Un service est
mis à disposition gratuitement sur [lessy.yuzu.ovh](https://lessy.yuzu.ovh) et le code est
[hébergé sur GitHub](https://github.com/marienfressinaud/lessy), le tout sous
[licence libre](https://github.com/marienfressinaud/lessy/blob/master/LICENSE).

La principale amélioration qui m’a le plus été demandée est la suppression des
contraintes sur les noms des projets : c’est désormais fait ! Il reste des
bricoles à peaufiner pour que ça ne pose plus du tout de soucis mais je verrai
en fonction des retours.

Ensuite, trois nouveautés liées aux tâches font leur apparition.

Tout d’abord, les tâches liées à des projets non démarrés ne sont plus listées
dans le backlog : le projet n’étant pas démarré, inutile de s’encombrer
l’esprit avec des tâches qu’on n’a pas besoin de réaliser. Il est toutefois
toujours possible de les planifier pour le jour même en se rendant dans le
projet, auquel cas celles-ci reprendront le comportement initial. Toutefois,
afin de ne pas oublier ces tâches, un indicateur a été ajouté à côté des
projets non démarrés.

![Un projet (Lessy) non démarré avec à sa droite le nombre de tâches associées](images/lessy/2017-12-29-aquarius-inbox-project.png)

La seconde nouveauté est que l’on peut désormais changer le projet auquel est
attachée une tâche. Très utile si comme moi vous oubliez de créer vos tâches à
partir des projets. La dernière nouveauté est la possibilité de transformer une
tâche en projet en deux clics. Si vous vous rendez compte que vos tâches
sont plus compliquées que prévu, c’est sans doute qu’il faut les redécouper ;
vous pourrez maintenant transformer celles-ci en projet puis leur créer des
tâches associées ! Ces deux fonctionnalités sont disponibles à partir du menu
contextuel, à droite des tâches.

![Le menu des tâches avec deux nouvelles options : "Attach to a project" et "Transform in project"](images/lessy/2017-12-29-aquarius-task-popover.png)

D’autres améliorations mineures sont aussi disponibles, le tout est détaillé
dans [l’annonce sur GitHub](https://github.com/marienfressinaud/lessy/releases/tag/aquarius).

Suite à la sortie de la version précédente (Apus), j’avais annoncé avoir ouvert
[un compte sur Liberapay](https://liberapay.com/Lessy). Suite à cette annonce,
trois personnes m’ont fait le plaisir de participer aux dépenses liées à Lessy
à hauteur de 1,35 € par semaine. Merci à elles et eux ! Plus que 65 centimes
par semaine et les frais du serveur + nom de domaine seront couverts :).

La prochaine version ([Aquila](https://github.com/marienfressinaud/lessy/projects/5)),
ne sera pas énorme mais devrait voir l’amélioration de la synchronisation
backend - frontend. Plus besoin de rafraîchir votre navigateur sur PC si vous
avez modifié des choses depuis votre tablette, tout devrait se faire
automatiquement !
