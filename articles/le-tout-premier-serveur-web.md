---
title: Le tout premier serveur Web
date: 2023-01-25 17:35
---

Il y a quelques jours, je suis allé à Genève et j’y ai visité le [CERN](https://fr.wikipedia.org/wiki/Organisation_europ%C3%A9enne_pour_la_recherche_nucl%C3%A9aire).
Outre des accélérateurs de particules, on y trouve cet ordinateur dans une vitrine (désolé pour la qualité de la photo) :

<figure>
	<img alt="" src="images/premier-serveur-web.jpg">

	<figcaption>
		<p>Une photo du tout premier serveur Web. Un autocollant est apposé dessus où l’on peut lire (en anglais) : « Cette machine est un serveur. NE L’ÉTEIGNEZ PAS !! »</p>

		<p>Un écriteau explique en dessous : « Cet ordinateur NeXT a été utilisé par Tim Berners-Lee, l’inventeur du <i lang="en">World Wide Web</i> (<abbr>WWW</abbr>) en 1989 lorsqu’il travaillait au CERN. Ce fut le premier serveur Web. À côté se trouve une copie du document dans lequel Tim Berners-Lee proposait le <i lang="en">Word Wide Web</i> : "<i lang="en">Information management: A proposal</i>". »</p>

		<p>Le document lui-même est illisible, mais on peut y lire une appréciation (en anglais) : « Vague mais excitant… »</p>
	</figcaption>
</figure>

Ça n’a pas fait immédiatement « tilt » dans ma tête, mais c’est vertigineux de se dire que ce petit machin est si proche de nous aussi bien en termes de lieu que de temporalité.
Le Web est à peine plus vieux que moi.