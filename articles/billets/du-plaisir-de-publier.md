---
title: Du plaisir de publier
date: 2022-06-22 20:14
---

Depuis quelques mois, j’écris tous les jours.
C’est parfois seulement un ou deux paragraphes ;
souvent des notes écrites à la va-vite.
L’exercice est quotidien et je m’y applique sans grand effort.

J’ai en revanche abandonné mes [publications journalières](publier-souvent.html).
J’ai essayé un rythme moins soutenu, mais je n’étais pas plus assidu.
Le plaisir n’y était pas.

Je n’ai jamais pris le sujet de la publication très au sérieux.
Ça n’a toujours été qu’une étape pénible entre l’écriture et le fait d’être lu.
Publier est une étape exigeante.
C’est en la considérant comme telle que j’y trouverai du plaisir.