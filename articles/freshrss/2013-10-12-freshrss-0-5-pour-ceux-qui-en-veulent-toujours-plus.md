Date: 2013-10-12 11:17
Title: FreshRSS 0.5, pour ceux qui en veulent toujours plus
Slug: freshrss-0-5-pour-ceux-qui-en-veulent-toujours-plus
Tags: php, freshrss
Summary: FreshRSS est le premier projet conséquent que j’ai porté. Initié en novembre 2012, la version 1.0 est sortie début 2015 seulement. J’annonçais peu après que je donnais les clés à la communauté pour reprendre le code [disponible sur GitHub](https://github.com/FreshRSS/FreshRSS). J’ai toutefois aujourd’hui toujours des idées pour l’améliorer.

FreshRSS continue son petit bonhomme de chemin 3 mois après la sortie de la version 0.4. C’est donc avec plaisir que je vous annonce une nouvelle version plus stable que la précédente et quelques nouveautés qui devrait être appréciées (comme d’habitude, évidemment).

![Logo de FreshRSS](http://freshrss.org/images/freshrss_logo.png)

## Ce que vous devez savoir...

... car il est toujours bon de le rappeler avant de continuer. FreshRSS est un agrégateur de flux RSS à auto-héberger, écrit en PHP et utilisant une base de données MySQL (ou MariaDB). Sa plus grande limitation est, à l’heure actuelle, qu’il n’est pas possible de gérer plusieurs comptes. Cela vient bien évidemment contre-balancer les points positifs :

* Une ergonomie pensée par et pour les utilisateurs
* Responsive design : FreshRSS s’adapte parfaitement à votre mobile
* Recherche par mots ou par #tags (et génération de flux RSS correspondants)
* Actualisation possible via CRON
* Possibilité de récupérer des flux RSS tronqués (à consommer avec modération !)
* Possibilité de changer de thème
* Marquage des articles comme lus au défilement de la page
* Et bien d’autres ! ☺

Une fois encore, la démo a été mise à jour et est accessible [à la même adresse](http://demo.freshrss.org).

## Les nouveautés

FreshRSS 0.5 c’est quelques 130 commits de plus que la 0.4, au moins [50 tickets de bugs fermés](https://github.com/FreshRSS/FreshRSS/issues?milestone=4&state=closed) (je pense que j’en oublie) et 3 mois de développement... Non je déconne, je suis loin d’avoir bossé dessus non-stop. Ça n’en reste pas moins, à l’heure actuelle, la plus grosse version que j’ai eu à développer, bien que n’ayant pas été seul cette fois-ci.

### Fonctionnalités

* Possibilité d’interdire la lecture anonyme
* Option pour garder l’historique d’un flux
* Lors d’un clic sur "Marquer tous les articles comme lus", FreshRSS peut désormais sauter à la prochaine catégorie / prochain flux avec des articles non lus. Cela évite bien des clics inutiles !
* Ajout d’un token pour accéder aux flux RSS générés par FreshRSS sans nécessiter de connexion. Cela permettra aussi d’accéder à l’API... prochainement ;)
* Possibilité de partager vers Facebook, Twitter et Google+
* Possibilité de changer de thème, le thème Flat design étant fourni en alternative au thème par défaut

### Design

* La police OpenSans est désormais appliquée
* Amélioration de la page de configuration
* Une meilleure sortie pour l’imprimante
* Quelques retouches du design par défaut
* Un nouveau logo

### Base de données

* Possibilité d’ajouter un préfixe aux tables lors de l’installation
* Ajout d’un champ en base de données (voir la partie sur la mise à jour de la base de données)
* Si possible, création automatique de la base de données si elle n’existe pas lors de l’installation
* L’utilisation d’UTF-8 est forcée. Cela peut entraîner **des problèmes d’encodage** lors de la mise à jour, j’en suis sincèrement désolé !

### Améliorations

* Le marquage automatique au défilement de la page a été amélioré
* La vue globale a été énormément améliorée et est beaucoup plus utile
* Amélioration des requêtes SQL
* Amélioration du Javascript
* Et des corrections de bugs en tout genre, comme d’habitude ☺

### Et ce qui manque

Malgré tout ça, je n’ai pas intégré tout ce que j’avais prévu au départ. On notera parmi les manques : la page de statistiques, la possibilité d’utiliser SQLite (bien que ça n’était pas dans mes plans initiaux, s’en est devenu une affaire personnelle !) et le nettoyage en profondeur du code Javascript.

Ces manques s’expliquent par le fait qu’il y a eu énormément d’ajouts que je n’avais pas prévu et que j’avais besoin d’une nouvelle version pour stabiliser le tout avant de passer à la suite.

## Mise à jour

Attention ! Cette nouvelle version inclut une mise à jour de la base de données et nécessite des actions de votre part.

### Base de données

Dans la table **`entry`**, il faut **supprimer** (si ils existent) les champs :

* `is_public`
* `lastUpdate`
* `annotation`

Dans la table **`feed`**, il faut **ajouter** le champ **`keep_history INT(1)`**.

Pour la prochaine version j’essayerai d’automatiser les mises à jour de base de données ce qui nous simplifiera la vie à tous ☺

### Application

une fois la BDD à jour, vous n’avez plus qu’à mettre à jour l’application comme d’habitude, à savoir :

1. [Téléchargez la nouvelle version](https://github.com/FreshRSS/FreshRSS/archive/0.5.0.zip)
2. Écrasez les fichiers des répertoires "app", "lib" et "public". Faites attention à garder vos anciens `app/configuration/application.ini` et `public/data/Configuration.array.php`
3. Supprimez le fichier `public/install.php` car vous n’en avez pas besoin dans le cas d’une mise à jour

Et n’hésitez pas à [remonter le moindre soucis](https://github.com/FreshRSS/FreshRSS/issues).

## Quelques remerciements

Clairement, cette version a été boostée par les contributions externes et je tiens à remercier tout particulièrement :

* Alexandre Alapetite ([Alkarex](https://github.com/Alkarex)) qui a proposé pas moins de 25 pull requests. Il a notamment amélioré les requêtes SQL, le code Javascript, le saut automatique aux catégories non lues suivantes ainsi que... beaucoup d’autres choses ! D’un coté je trouve ça un peu fou que quelqu’un ait pu passer autant de temps à améliorer FreshRSS, de l’autre je trouve ça génial ! ☺
* [Cypouz](https://github.com/Cypouz) qui a proposé le nouveau logo beaucoup plus sympa que le précédent.
* [Vincent](http://influence-pc.fr/) qui a aussi un peu participé au logo, mais qui est surtout très encourageant et remonte régulièrement des soucis.

J’aimerais d’ailleurs faire remarquer que c’est grâce aux différentes contributions que le projet évolue le plus. Sans cette aide externe et ces retours, sans doute que FreshRSS serait déjà abandonné car je ne verrais pas l’intérêt d’en faire plus. Si c’est le développeur qui insuffle la vie à un projet, c’est la communauté autour qui le fait vivre. Merci donc à tous pour votre aide !

## À propos du logo

Tout logo se doit de représenter une idée, un concept. Extraits [des discussions](https://github.com/FreshRSS/FreshRSS/issues/119) à propos du nouveau logo :

![Le logo FreshRSS sous différentes tailles](images/freshrss/logos.png)

Le nouveau logo de FreshRSS

> J’ai finalement gardé le logo RSS (principe central du programme), entouré d’autres logo RSS (les autres sections des cercles, symbolisant la multitude de flux à suivre), le tout sous forme circulaire concentrique (symbolisant l’aspect rassembleur du programme, qui centralise les flux suivis). Le côté « fresh » est rappelé par la couleur bleu.

> L’avantage [de ce logo] [...] est aussi qu’il est ouvert. [...] Le fait qu’il soit ouvert peut sous-entendre beaucoup de choses : l’ouverture du code source, l’invitation à participer, une porte pour les flux RSS qui peuvent ainsi « entrer », etc.

> Le rond pourrait représenter le noyau qu’est FreshRSS et qui agrège les différents flux

## Vers la 0.6 et au-delà !

Comme à chaque publication de nouvelle version je rajoute une nouvelle étape avant la version 1.0, je ne ferai donc pas de pronostics cette fois-ci sur une version stable. Voici néanmoins mes plans pour la prochaine version qui sera donc la 0.6 :

* Ajout de la page de statistiques prévue pour la 0.5 ([#90](https://github.com/FreshRSS/FreshRSS/issues/90))
* Ajout d’une API "compatible Google Reader" ([#13](https://github.com/FreshRSS/FreshRSS/issues/13))
* Gestion de SQLite en plus de MySQL (+ PostgreSQL ?) ([#100](https://github.com/FreshRSS/FreshRSS/issues/100))
* Refonte du code Javascript ([#121](https://github.com/FreshRSS/FreshRSS/issues/121))
* Permettre l’import / export des articles favoris ([#163](https://github.com/FreshRSS/FreshRSS/issues/163))
* Possibilité de partager un article vers Diaspora* et Poche ([#175](https://github.com/FreshRSS/FreshRSS/issues/175))
* Script de mise à jour automatique de la base de données

Du gros boulot en perspective donc, mais qui devrait me rapprocher toujours plus d’une version finale. Si vous avez des demandes, n’hésitez pas à m’en faire part [sur Github](https://github.com/FreshRSS/FreshRSS/issues). Pour le moment les seules demandes que j’ai refusées (ou reportées à une version ultérieure à la 1.0) sont la gestion multi-utilisateurs et une interface alternative de connexion : ça demanderait beaucoup trop de boulot en profondeur.

J’ai aussi dans mes cartons l’idée de fournir un wiki sous forme de documentation utilisateur ou de FAQ pour présenter les différentes options de FreshRSS et expliquer comment l’utiliser au maximum de ses capacités. Le dernier délai pour m’en occuper est la date de sortie de la version 1.0... autant dire que j’ai le temps :D

## En guise de conclusion

J’espère que FreshRSS saura vous satisfaire autant qu’il me satisfait moi-même. Je n’ai pour le moment eu aucun retour réellement négatif, j’ai quelques utilisateurs pleinement (?) satisfaits et je vois même fleurir sur Internet, de temps en temps, quelques articles parlant de mon agrégateur. Ce qui est, disons-le, très bon pour mon ego ☺

Aussi, je vais bientôt fêter les un an du projet (le premier commit date du 21 octobre 2012) et c’est toujours encourageant de voir que de plus en plus de personnes trouvent de l’utilité dans mon travail. Travail qui était au départ totalement personnel et n’était pas parti pour durer dans le temps. Force est de constater que FreshRSS a mué peu à peu vers un projet un peu plus communautaire et ouvert et je suis pleinement satisfait de cette transformation.

## Edit

Suite à un bug assez important dans la gestion des catégories (les catégories vides n’apparaissaient plus), je suis passé en version 0.5.1. J’en ai profité pour inclure quelques patchs de traduction et correction ;)
