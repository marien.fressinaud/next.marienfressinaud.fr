---
title: Publier souvent
date: 2022-02-22 12:20
---

Ça va bientôt faire un mois que je me suis fixé [l’objectif de publier un billet par jour](billets-courts.html). Le rythme était totalement arbitraire et je ne l’ai — évidemment — pas tenu : je ne tiens jamais ce genre de choses.

**Le résultat est tout de même positif puisque j’ai publié 20 billets-articles : 17 ici et 3 dans [le carnet de Flus](https://flus.fr/carnet/).**

Certains jours, j’ai également rédigé des billets sans les publier : soit pour le faire plus tard, soit parce que je n’avais plus envie de les publier. 

Quelques billets rédigés à l’avance se trouvent dans un état intermédiaire : je ne sais pas quoi en faire. Dans ce genre de cas, il m’est arrivé de les réécrire de zéro. Ça a été le cas de mon billet « [Pour bâtir un morceau de Web](pour-batir-un-morceau-de-web.html) ».

J’ai réalisé que, même pour de très courts billets, je pouvais passer beaucoup de temps de relecture. Après rédaction, j’ai encore corrigé et reformulé [le billet concernant Apostrophe](apostrophe.html) pendant 30 minutes ; il fait quatre paragraphes. J’adore cette étape, mais j’ai parfois l’impression d’y perdre en spontanéité.

En partageant des choses, on bénéficie également des lumières des autres. Je connais François depuis longtemps, mais je ne savais pas qu’il me lisait : il m’a corrigé [ma requête <abbr>SQL</abbr> infernale](exploration-sql.html) (<i lang="la">confer</i> la mise à jour en bas de page). Quant à lui, il devrait désormais utiliser l’attribut `lang` sur les balises <abbr>HTML</abbr> `<i>` pour indiquer les mots de langues étrangères 🙂

Publier, c’est utiliser son site. Le faire de manière régulière a été l’occasion d’améliorer un certain nombre de choses dessus : [l’optimisation des images](yoga.html), [l’ajout d’un thème sombre](theme-sombre.html) et [de Turbolinks](turbolinks.html), [le support de technos issues de l’IndieWeb](indieweb.html) et d’autres trucs qui n’ont pas eu le droit à un article. 

**Dans l’ensemble j’ai pris du plaisir à rédiger. Je suis également content d’avoir publié certaines choses sans me poser la question de savoir si ça intéresserait d’autres personnes : je publie pour moi avant tout.**

Je vais conserver cet objectif d’un billet par jour, toujours sans le tenir. J’ai envisagé de viser un billet par semaine, mais je sais que rater un objectif atteignable me démotiverait bien plus que de rater un objectif que je _sais_ inatteignable.