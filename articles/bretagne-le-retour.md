---
title: Bretagne, le retour
date: 2019-10-21 11:50
---

Je suis rentré de [Bretagne](bretagne.html) un peu plus tôt que prévu. J’ai
finalement beaucoup moins randonné que ce que je voulais, mais j’ai dû me
rendre à l’évidence de trois choses :

1. la santé physique, ça se soigne ;
2. un budget, ça explose vite ;
3. marcher seul, c’est long.

C’était néanmoins une chouette balade entre le Mont-Saint-Michel et
Saint-Brieuc, de superbes paysages (et des moins bien aussi, mais peu 😉),
quelques rencontres qui font plaisir (en particulier un groupe de quatre que
j’ai suivi presque tout du long), et des bonnes galettes. Je remercie également
Hervé de m’avoir montré Brest, ainsi que Thomas et Aline pour l’hébergement et
les croissants !

Une chose m’a surpris : la Bretagne, c’est pas plat du tout ! Ça monte et ça
descend sans cesse, avec des marches (j’en ai compté 106 à Saint-Cast-le-Guildo
en fin de journée pour atteindre l’hôtel, pas cool) ou sans. J’avais clairement
mal anticipé ça et mes tendons d’Achille ont souffert plusieurs jours de suite
(d’où le fait de m’arrêter pour éviter de me blesser).

La période à laquelle j’y suis allé n’était pas idéale, les établissements
commencent à fermer et il était parfois compliqué de se loger. Au pire ça
aurait été camping sauvage dans un coin, mais je voulais éviter. Je viserai la
prochaine fois la première quinzaine de septembre (des contraintes m’en ont
empêché cette fois-ci).

À propos de la météo (comme la question revient souvent), j’ai eu droit à
quelques bonnes averses, mais je n’ai réellement fini trempé que deux fois.
J’étais équipé de vêtements qui sèchent rapidement donc, en général, le simple
fait de marcher suffisait pour redevenir sec (le vent aidant bien). J’ai quand
même eu du très beau temps la majorité du voyage, me permettant d’être en
t-shirt.

Je me suis amélioré dans la préparation de mon sac par rapport à mes voyages
précédents. Bien qu’étant parti avec un sac d’environ 14 kg, j’aurais pu gagner
en m’évitant le matériel de camping (3 kg au total dont j’ai eu peu besoin) et
en emportant moins de nourriture au départ. Concernant ce deuxième point, il
est possible soit de manger au resto, soit de se ravitailler dans des
supérettes à intervalle régulier (ce n’est pas un problème sur ce chemin de
randonnée). J’ai calculé, je pense pouvoir viser un sac de 9 kg aisément, voire
encore moins si je veux vraiment voyager léger.

Pour finir, j’aimerais bien retourner en Bretagne pour finir ce que je n’ai pas
fait et visiter d’autres coins. En revanche je ferai en sorte de ne pas voyager
seul et de mieux me préparer physiquement avant de partir. Et si vous étiez là
pour les photos : je n’en ai pas prises, désolé !
