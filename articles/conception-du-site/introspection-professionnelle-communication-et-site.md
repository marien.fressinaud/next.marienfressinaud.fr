---
title: Introspection professionnelle : communication et site
date: 2019-01-11 11:45
---

Pour ce dernier article de mon « introspection professionnelle », je vais
aborder le sujet de la communication et dessiner une esquisse de ce que sera
l’architecture du site. Pour rappel, [le premier article](introspection-professionnelle-valeurs-et-raison-detre.html)
définissait mes valeurs et ma raison d’être, tandis que [le second](introspection-professionnelle-competences-et-objectif.html)
décrivait les compétences que je souhaite appliquer dans mon futur métier ainsi
que l’objectif que je me fixe.

Les choses commencent doucement à se clarifier pour moi, ce troisième article
aborde enfin la phase concrète de la démarche et **les premiers traits du site
seront tirés dans la dernière partie**. J’ignore encore qu’elles seront les
missions sur lesquelles je serai amené à travailler, mais l’accueil qui a été
réservé à mes deux premiers articles me conforte dans la voie que j’ai commencé
à emprunter.

## Différents supports pour communiquer

J’ai commencé à me poser la question du support de communication lorsque j’ai
réalisé la diversité des médias que j’utilisais jusque-là : mon site
évidemment, Twitter, Mastodon, Diaspora\* (plus trop maintenant), mais aussi
GitHub, sans compter les différents blogs ou espaces où j’ai été amené à écrire
au compte-goutte. Je me posais régulièrement la question de quelle information
devait être partagée où et sur quel ton. Il est grand temps de remettre tout
cela à plat.

Pour commencer, parlons de ce dont je ne veux plus avoir à m’occuper : GitHub
et Gitlab, et en particulier du premier. Il s’agit là de forges logicielles,
c’est-à-dire d’espaces où l’on dépose du code source, agrémentés d’outils pour
contribuer à ce code (notamment un <span lang="en">_bugtracker_</span> ainsi
qu’un système de <span lang="en">_pull requests_</span> permettant de proposer
des modifications du code). Mais ce n’est pas tout : GitHub en particulier
n’est pas une simple forge, ce qui en fait sa force se trouve aussi dans ses
aspects communautaires. On y met en avant les projets sur lesquels on
travaille, notre activité quantifiée sous forme de beaux diagrammes et on peut
même suivre ou être suivi par d’autres développeurs et développeuses pour voir
ce qu’ils font. Gitlab est sans doute légèrement moins orienté vers cela, mais
intègre tout de même certains de ces méchanismes. Le défaut de ces deux
plateformes, c’est qu’elles sont orientées exclusivement vers le code, vers la
technique. Si vous avez suivi mes articles précédents, vous aurez compris que
cela m’intéresse assez peu au final. Quantifier le nombre de lignes de code ou
de tickets que j’ai ouverts ne m’intéresse pas, cela flatte tout juste l’égo
lorsque l’on est « productif ». Utiliser GitHub ou Gitlab pour communiquer
m’apparaît donc comme **trop restrictif et orienté vers un but qui n’est pas le
mien**. Je ferai donc désormais le minimum pour maintenir mes profils à jour
sur ces plateformes.

Ensuite, le couple Twitter / Mastodon est légèrement plus compliqué. Ces deux
médias sociaux se ressemblent énormément dans leur mode de fonctionnement
(c’est-à-dire le partage de courts messages instantanés). Toutefois, Mastodon
possède une communauté plus petite et plus ouverte où l’on trouve des personnes
avec qui échanger de façon plus proche. Twitter me fait l’impression d’une
population très (trop ?) hétérogène, où n’importe qui peut s’immiscer dans une
conversation dans laquelle il n’est pas le ou la bienvenu·e (et s’y accrocher
en plus, le bougre !) Dans les deux cas, l’argumentation est compliquée de part
la limite du nombre de caractères dans les messages (280 sur Twitter, 500 sur
Mastodon), alors **autant privilégier la plateforme qui offre le cadre le plus
accueillant pour échanger**. Par conséquent, j’utiliserai Mastodon pour
partager des choses professionnelles et plus personnelles. Je conserverai
toutefois mon compte Twitter pour partager des choses exclusivement
professionnelles et pour continuer ma veille de l’actualité. Quant à mon compte
Diaspora\* qui est à l’abandon depuis un moment, il risque de disparaître pour
de bon un de ces jours.

Le dernier support de communication d’importance que je souhaite utiliser est
bien entendu mon site Internet. Je le souhaite au centre de mes partages, en
espérant écrire plus qu’auparavant (la fin de l’année 2018 m’a montré que j’en
étais capable). Je détaille plus en détails comment j’imagine mon site dans la
dernière partie de cet article.

## Les petits à-côtés de la communication

En travaillant les supports de communication, quelques éléments sont ressortis
que je n’ai pas encore traités.

En premier lieu, le <abbr>CV</abbr> est un support que je trouve intéressant.
Il est surtout utilisé pour postuler en tant que salarié, mais aussi pour faire
de la prestation chez des clients. J’aime bien y jeter un coup d’œil pour
savoir comment les gens se présentent de manière succincte. D’un point de vue
plus personnel, je trouve qu’il s’agit d’un exercice de synthèse assez
intéressant et j’aime bien me limiter à une page <abbr>A4</abbr> bien
qu’aujourd’hui j’aurais sans doute matière à déborder un peu. Il ne s’agit
toutefois pas d’un besoin urgent, bien que je m’y pencherai sans doute avec
intérêt d’ici quelques mois.

Un autre sujet facultatif dans l’immédiat est celui des cartes de visite. Je
vais sans doute rencontrer des gens qui chercheront à me contacter et la carte
est dans ces cas-là intéressante pour donner les informations essentielles.
Je devrais toutefois être capable de découper un bout de papier dans un coin de
nappe et y écrire mon nom ainsi qu’une adresse courriel. Il manquera l’aspect
« objet à collectionner », mais je suis persuadé que les gens sauront s’en
passer. La fin du monde [est pour demain](https://fr.wikipedia.org/wiki/Th%C3%A9ories_sur_les_risques_d%27effondrement_de_la_civilisation_industrielle)
après tout.

En parlant d’adresse courriel, se pose la question du moyen de me contacter. Je
ne privilégie pas Twitter et je risque d’oublier des choses si les gens me
contactent par Mastodon. De plus, j’aime centraliser ma correspondance, et le
courriel est idéal pour cela. Ainsi, je compte mettre en évidence sur mon site
une adresse unique pour me contacter. Certains préfèrent passer par un
formulaire pour éviter que des personnes ne récupèrent leur adresse et limiter
ainsi le spam, mais la mienne traîne déjà depuis plusieurs années sur mon site
et j’arrive à peu près à limiter le spam. Je me suis aussi posé la question de
proposer un numéro de téléphone, à Sogilis cela était fait car certains clients
appellent plus volontiers qu’ils ne rédigent un courriel. Le téléphone est
toutefois pour moi un élément relativement intrusif de par la présence physique
qu’il requiert et je le réserve à un cadre privé.

Enfin, le dernier sujet que je souhaitais aborder est la photo de profil. Si je
ne compte pas en mettre une en évidence sur le site, j’apprécie toujours de
pouvoir me faire une idée du visage d’une personne avant de la rencontrer, ne
serait-ce que pour la reconnaître plus facilement. La photo que j’utilise
aujourd’hui commence à dater un petit peu, et les personnes qui m’ont croisé
récement seront sans doute d’accord pour dire qu’elle n’est plus vraiment au
goût du jour. J’ai vu à plusieurs reprises le conseil de passer par un
photographe professionnel, mais je vais encore laisser le sujet de côté
quelques semaines avant de me décider sur ce que je fais.

## L’architecture du site, pour me représenter

Mon futur site aura deux objectifs : me servir de vitrine professionnelle (et
donc me présenter) et à partager ce sur quoi je travaille. La structure de base
de la page d’accueil que j’envisage n’a rien de très originale :

1. présentation en une phrase de **qui je suis**, ainsi que mise en avant de
   **mon objectif** ;
2. énumération de **mes compétences** pour indiquer sur quoi je peux travailler
   et comment ;
3. présentation de **mon manifeste** pour mettre en évidence mes valeurs et
   préciser que je ne travaille pas sur n’importe quoi ;
4. indication de **mon adresse courriel** ainsi que **les médias sociaux** sur
   lesquels on peut me trouver ;
5. enumération **des projets** sur lesquels j’ai travaillé et que je souhaite
   mettre en avant (limités à environ 5) ;
6. enfin, un pied de page pour servir de rappel (liens vers les différentes
   pages du site, rappel des informations de contact, etc.)

Il existera des pages supplémentaires pour détailler certaines parties,
notamment mes compétences et mon manifeste (qu’il me reste à écrire !)
J’intégrerai aussi au niveau de la liste des projets, un lien amenenant vers ma
page « [<span lang="en">now</span>](https://marienfressinaud.fr/now.html) » que
je tiens à jour pour indiquer ce sur quoi je travaille en ce moment.

J’aimerais limiter le nombre de liens dans la barre de navigation en haut de la
page, et je me contenterais d’ailleurs bien d’un simple lien vers le blog. Au
passage, j’essayerai d’effacer la frontière qui peut exister habituellement
entre la partie site et le blog. Il paraîtrait en effet logique qu’un lien
partant de la liste des projets amène vers une série d’articles liée à un
projet en particulier. La page de blog en elle-même se contenterait de
présenter la liste complète des articles, mais deviendrait optionnelle pour
accéder aux articles. Je souhaite de ce fait les placer dans leur contexte.

Afin d’illustrer ce vers quoi je veux tendre, **j’ai créé [une page de
démonstration](demo-prochain-site/index.html)** relativement complète. Avec
cela, j’ai déjà une bonne base de travail pour avancer rapidement. Il me reste
encore pas mal de contenu à écrire et [<span lang="en">Boop!</span>](https://framagit.org/marienfressinaud/boop),
mon générateur de sites statiques, va encore devoir évoluer un peu, mais je
peux dorénavant envisager une mise en ligne à la fin du mois.
