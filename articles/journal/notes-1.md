---
title: Notes #1
date: 2020-01-27 15:00
blog: false
---

[Thomas](https://détour.studio/weeknotes/1/) a commencé à publier des
« weeknotes » début janvier. J’avais déjà adopté des notes légèrement
similaires dans le rythme de publication et dans la forme, mais je les gardais
pour moi. Je fais évoluer mon format pour les mettre sur mon site, mais sans
les afficher sur la page d’accueil ni dans le flux RSS : c’est du semi-privé à
destination de Mastodon. Ces notes seront potentiellement amenées à devenir
publiques plus tard. Elles viennent également remplacer ma page « [now](now.html) »,
dont j’avais découvert le format [chez Claire](https://www.clairezuliani.com),
mais qui ne me convenait que moyennement.

Pour l’instant je garde un format libre pour me laisser l’espace
d’expérimenter. Le rythme prévu est hebdomadaire, mais je ne me fixe pas de
contrainte là-dessus (d’où le fait que le « week » ait sauté du nom de ma note).

Mes objectifs avec ce format sont de :

- documenter des « pensées » qui pourraient être utiles à d’autres ;
- m’entraîner à écrire plus ;
- faciliter une certaine prise de recul pour mon moi-futur.

---

J’ai appelé [Clara](https://www.clara-chambon.fr/) mercredi dernier pour qu’on
commence à travailler sur un logo pour [Flus](https://flus.fr). Je sens qu’à
travers nos échanges et ceux que j’ai eus avec d’autres personnes, mon projet
prend une autre forme, inattendue mais tellement plus enthousiasmante que celle
actuelle.

Le même jour j’ai réalisé mon premier pain qui a correctement gonflé à la
cuisson, enfin ! J’ai par contre changé tellement de choses dans ma manière de
faire que je ne suis pas certain de ce qui en est à l’origine. Les deux pistes
les plus probables (et complémentaires) sont : un temps plus long de repos pour
la pâte et le pré-chauffage du plat sur lequel je fais cuire le pain. Le pain
était par contre raté : j’ai changé de sel et celui-ci est horrible (je ne
savais même pas que c’était possible !) Ça passe avec du beurre salé ou du
fromage.

Je me suis rendu à [Snowcamp](https://snowcamp.io) les jeudi et vendredi qui
ont suivi. Les conférences y sont généralement de plutôt bonne qualité et
cette année n’a pas dérogé à la règle. Celles que je retiens particulièrement :

- « [The Sound of Silence: Des APIs Web pour l'accessibilité des déficiences
  visuelles et auditives](https://snowcamp2020.sched.com/event/XoOU) »
- « [Tests de propriétés : Ecrivez moins de tests, trouvez plus de
  bugs](https://snowcamp2020.sched.com/event/XoOj) »
- « [Le Spleen du Mainteneur](https://snowcamp2020.sched.com/event/XoP4/) »
- « [In our documentation we trust ! REX sur l'intégration de l'écriture de la
  doc dans le cycle de developpement](https://snowcamp2020.sched.com/event/XoPh) »

Aujourd’hui était le jour de démarrage d’un nouveau cycle de travail.
Habituellement c’est le moment où je me fixe des objectifs pour les six
semaines à venir, mais j’ai laissé tomber cette fois-ci. J’ai réalisé que les
objectifs étaient souvent artificiels, que les atteindre ne m’apportait pas
grand-chose et surtout que je me sentais mal si je ne les atteignais pas.
J’aime sentir mes méthodes de travail évoluer de la sorte : lorsque les
« bonnes » idées d’un instant T se révèlent en fait contre-productives et que
je réalise que rien n’est figé dans le marbre.

Ironiquement, je travaille en parallèle sur la prochaine version de
[Lessy](https://lessy.yuzu.ovh) qui a justement pour but de figer un certain nombre
de mes pratiques actuelles dans un outil pour les rendre plus pratiques. Je
n’avance pas bien vite vu que je bosse dessus les soirs et les weekends, ça me
laisse encore un peu de temps pour sentir si mes méthodes de travail sont
bonnes ou non.
