---
Date: 2018-05-31 00:10
Title: Un homme sans rêve
Slug: un-homme-sans-reve
Tags: Asmara, nanowrimo
Summary: 3 ans et demi plus tard, il est grand temps de commencer à en dévoiler un peu plus sur l’univers d’Asmara avec la parution d’une nouvelle directement extraite du bouquin que j’avais écrit à l’occasion du NaNoWriMo 2014.
---

Écrire un bouquin de 50 000 mots en un mois, c’était le défi que je m’étais
fixé en [novembre 2014](https://marienfressinaud.fr/en-novembre-cest-nanowrimo.html).
Ces mots, je ne les ai jamais publiés nulle part. Ils étaient pourtant là,
couchés sur de multiples octets, ces 50 000 mots et le défi du
[NaNoWriMo](https://fr.wikipedia.org/wiki/National_Novel_Writing_Month)
relevé. Mais l’excitation redescendue, les deux pieds à nouveau sur Terre, il
fallait se rendre à l’évidence : ce n’était vraiment pas terrible. Des fautes,
des incohérences, des dialogues pourris, des éléments de l’intrigue avec
lesquels je n’étais pas à l’aise… et même une intrigue pas intrigante pour un
sou !

Pourtant, au milieu de tout ça il y avait forcément moyen de sauver quelques
meubles. En 50 000 mots, il y a forcément des fulgurances, des choses qui
fonctionnent mieux que d’autres, un ensemble de mots et de phrases qui
« sonnent » bien. C’est en mars 2016 que je relus l’histoire d’*Asmara*, avec
beaucoup de recul et que je reprenais un certain plaisir à me plonger dans un
univers que j’avais créé (hey ! C’est pas rien non plus). Un chapitre en
particulier (le dernier écrit) retint mon attention : il y avait matière à en
tirer une mini-nouvelle.

Quelques relectures et corrections plus tard, elle était prête. Cela m’avait
pris seulement 9 jours à retravailler ce court texte, mais il me fallut deux
ans de plus pour trouver le courage de le publier. Alors voilà, je pose tout ça
là, ignorant si ma nouvelle sera lue ou non. Pour moi il s’agit surtout de
mener un peu plus loin ce que j’ai entamé il y a 3 ans et demi avec le
NaNoWriMo, bien qu’il reste encore beaucoup de chemin à parcourir avant d’en
dévoiler plus sur *Asmara*.

La nouvelle se nomme *Un homme sans rêve* et est placée sous licence [CC BY](https://creativecommons.org/licenses/by/4.0/).

- [fichier PDF](files/Un_homme_sans_reve.pdf)
- [fichier EPUB](files/Un_homme_sans_reve.epub)
- [le dépôt Git](https://framagit.org/marienfressinaud/un-homme-sans-reve)
  (l’historique peut être intéressant si vous vous questionnez sur le processus
  de réécriture)

Ce chapitre/nouvelle est en fait une réinterprétation d’un chapitre de
[*1984*](https://fr.wikipedia.org/wiki/1984_(roman)) de Georges Orwell, toute
ressemblance n’est absolument pas fortuite. Le défi lors de cette réécriture
était surtout de travailler les dialogues et l’évolution psychologique du
personnage. Comme la nouvelle s’inscrit dans un contexte plus global, il
subsiste des références au reste de l’histoire que vous ne saisirez
probablement pas. J’ai décidé de les conserver pour me laisser des pistes à
explorer de nouveau lorsque je souhaiterai me replonger dans *Asmara*.

Je vous souhaite une bonne lecture !
