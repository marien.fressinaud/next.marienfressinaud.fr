Date: 2013-07-04 20:38
Title: FreshRSS 0.4, une release qu’elle est bien
Slug: freshrss-0-4-une-release-qu-elle-est-bien
Tags: php, freshrss
Summary: FreshRSS est le premier projet conséquent que j’ai porté. Initié en novembre 2012, la version 1.0 est sortie début 2015 seulement. J’annonçais peu après que je donnais les clés à la communauté pour reprendre le code [disponible sur GitHub](https://github.com/FreshRSS/FreshRSS). J’ai toutefois aujourd’hui toujours des idées pour l’améliorer.

Cher toutes et tous,

séchez vos larmes, l’an 0 après Google Reader vient de commencer. Séchez vos larmes car vient vers vous une foultitude de nouveaux agrégateurs qui ont essayé de rivaliser ces derniers mois en terme de choix, de fonctionnalités et d’ergonomie. Séchez vos larmes vous dis-je car [FreshRSS vient de sortir en version 0.4](http://freshrss.org) et vous apporte plein de bien belles nouveautés.

## Les infos utiles

FreshRSS est un agrégateur de flux RSS à auto-héberger sur son propre serveur. Il est relativement rapide et puissant. Il se différencie des autres agrégateurs par quelques fonctionnalités bien utiles et une ergonomie pensée pour un usage quotidien. À titre d’exemple, quelques points forts de mon application :

- Une version mobile (que j’adore !)
- Chaque recherche (par mot-clé ou par tag) possède son propre flux RSS. Vous pouvez donc vous abonner à un flux RSS généré par FreshRSS avec tous les articles contenant le mot "Linux".
- Actualisation possible par CRON
- Internationalisation : anglais et français supportés
- Marquage des articles comme lu au défilement de la page (optionnel)
- Différents modes d’affichage (normal, lecture et global)
- Chargement des images en mode lazyload (chargement uniquement lorsque l’image doit apparaître à l’écran)
- Possibilité de récupérer des flux RSS tronqués
- Et bien d’autres !

Qui dit nouvelle version, dit mise à jour de la démo, [toujours accessible à la même adresse](http://demo.freshrss.org) pour les plus pressés.

## Les grandes nouveautés

**Ajout de deux modes de vue**

Jusque là, il n’existait qu’un mode de lecture "par défaut" permettant de parcourir, lire, mettre en favoris, etc. les articles. Désormais il y aura aussi la "vue lecture" qui se veut plus agréable quand vous ne souhaitez que lire (pas marquer comme lu ni rien. Juste lire). La troisième vue est la "vue globale" vous permettant de voir d’un coup d’œil les flux possédant des articles non lus. Cette dernière est largement perfectible mais a le mérite d’exister.

Personnellement je continue d’utiliser la vue normale par défaut car plus pratique **pour moi**, mais sur mobile, la vue lecture est plus agréable par exemple.
Le but est de vous proposer différentes approches de FreshRSS pour ne pas vous enfermer dans un mode de fonctionnement unique. Je suis ouvert à toutes suggestions d’amélioration de ces vues, notamment la vue globale que je trouve finalement assez inutile en l’état, bien que j’ai déjà des idées pour l’améliorer.

**Possibilité d’optimiser la base de données**

L’optimisation de la base de données est une chose d’assez peu connue, et je pense très rarement utilisée dans les applications web "basiques". Ceci dit, à l’aide d’un simple bouton vous pourrez désormais réduire la taille utilisée par votre base de données : je suis passé d’une base de 28Mo à une base de 20Mo. En revanche, j’ignore si toutes les bases de données supportent bien cette commande SQL et je suis intéressé par d’éventuels retours !

Pour information, la requête SQL qui se cache derrière tout ça est ([voir la doc MySQL](https://dev.mysql.com/doc/refman/5.5/en/optimize-table.html)) :

```sql
OPTIMIZE TABLE nom_de_la_table
```

**Mode endless**

Le mode endless fait son grand retour. Ce mode permet de charger les articles suivants le dernier de la liste en les rajoutant à la suite, sans avoir à recharger la page complètement.

Ça marche beaucoup mieux qu’avant (il y avait des soucis quand on marquait les articles comme (non) lu) mais il se peut qu’il reste quelques soucis car je ne l’utilise pas trop de mon côté.

**Chargement des images en mode lazyload**

Cette fonctionnalité j’en suis particulièrement content parce que j’étais très dubitatif sur son intérêt avant de l’intégrer... puis finalement je ne peux plus m’en passer !

Le principe est de ne charger les images des articles qu’à partir du moment où elles sont censées apparaître à l’écran. Le gain de temps au chargement de la page est énorme. Le seul soucis est que l’on se retrouve de temps en temps avec un rectangle gris, le temps de charger l’image. Pour cela, il est possible de désactiver la fonction de lazyload, mais je vous conseille quand même de la garder ;)

**Possibilité de marquer les articles comme lu au défilement**

Là encore une fonctionnalité très intéressante. Grâce à celle-ci, il est possible de marquer un article comme lu lorsque vous en avez lu à peu près les 3/4. Cette fonctionnalité marche très bien lorsqu’on lit avec les articles "dépliés" par défaut, mais peut se coupler aussi avec les articles pliés. Il ne tient qu’à vous de définir les réglages qui vous conviennent le mieux !

**Et bien d’autres !**

Des corrections de bugs, des améliorations au niveau du design et de l’ergonomie, des petits ajouts par-ci par-là. Je vous laisse découvrir cette nouvelle version qui devrait plaire à pas mal d’entre vous ☺

## Mettre à jour votre FreshRSS

Comme pour le passage de la v0.2 à la v0.3, la mise à jour devrait être assez facile puisqu’il n’y a pas de modification de la base de données.

1. [Téléchargez la nouvelle version](https://github.com/FreshRSS/FreshRSS/archive/0.4.0.zip)
2. Écrasez les fichiers des répertoires `app`, `lib` et `public`. Faites attention de garder vos anciens `app/configuration/application.ini`, `public/data/Configuration.array.php` et tout fichier que vous avez pu modifier de votre côté.
3. Enfin, supprimez le fichier `public/install.php` car vous n’en avez pas besoin dans le cas de la mise à jour (sauf si vous tenez à refaire l’installation :p)
4. Un soucis ? [Nos équipes se chargent de vous répondre au plus vite.](https://github.com/FreshRSS/FreshRSS/issues)

## Fonctionnalités futures (v0.5 ou v1.0 ?)

Autant vous le dire, je pense que FreshRSS n’a plus besoin de beaucoup d’ajouts. Aussi, si je n’ai pas trop de demandes pour la prochaine version, il se peut qu’il s’agisse de la version 1.0 (enfin !)

Et donc, au menu ce que je vous prépare (pas grand chose au final) :

- Un sélecteur de thèmes ([bug 86](https://github.com/FreshRSS/FreshRSS/issues/86)). J’essayerai de proposer au moins 3 thèmes différents pour varier les goûts.
- Une page de statistiques ([bug 90](https://github.com/FreshRSS/FreshRSS/issues/90)). Au delà du côté bling-bling de la fonctionnalité, je pense que ça peut être utile (/ intéressant) de voir quels sont les sites qui publient le plus, quels jours, etc. J’essayerai de ne pas surcharger en graphiques tout de même ;)
- Correction de bugs... comme d’habitude !
- Amélioration des nouvelles vues
- Et je pense que ce sera tout.

Par la suite (si je m’arrête là) j’assurerai évidemment la maintenance pour corriger les bugs, mais je ne pense pas ajouter de nouvelles fonctionnalités... si vous êtes intéressés par des nouveautés, faites m’en part au plus vite !

## N’oublions pas les autres

Car si FreshRSS ne vous convient pas, il existe bien d’autres agrégateurs. Donnez au moins une chance à [Kriss Feed](https://github.com/tontof/kriss_feed) ou [Leed](http://projet.idleman.fr/leed/) qui évoluent aussi pas mal de leur côté avec des développeurs à l’écoute des remarques qu’on peut leur faire (c’est important !). Je suis fan du côté facilité d’installation de Kriss Feed et du système de plugins de Leed (ce ne sera pas suffisant pour me faire abandonner FreshRSS ceci dit :p)

## Remerciements

J’aimerais remercier toutes les personnes qui m’ont fait des retours ou des suggestions, notamment [4nti-7rust](https://github.com/4nti-7rust) pour l’idée du marquage au défilement de la page, [rplanelles](https://github.com/rplanelles) pour l’idée du chargement des images en mode lazyload et [Vincent](http://influence-pc.fr/) pour parler de FreshRSS un peu partout !

Je n’oublie pas les autres qui m’ont demandé de l’aide par mail, notamment la courageuse personne qui a essayé d’installer FreshRSS sur un espace Free (pour information, il n’y a pas PDO sur les serveurs de Free apparemment, on ne peut donc pas y installer FreshRSS -_-)

J’en profite pour conclure que c’est vraiment agréable d’avoir tous ces retours car ça motive le développement et me permet de savoir pour "qui" je travaille (c’est encore plus gratifiant que [les 15 étoiles sur Github](https://github.com/FreshRSS/FreshRSS) :p)

## Le mot de la fin

Agrégateur. (et non pas a**gg**régateur comme je peux le voir régulièrement !)
