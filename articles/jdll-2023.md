---
title: JdLL 2023
date: 2023-03-30 19:00
---

J’avais déjà fait une première annonce le mois dernier sur le blog de Flus : **je serai présent aux [JdLL](https://jdll.org/), ce weekend à Lyon.**

J’y donnerai deux conférences.

La première se nomme « **Bileto : un produit & une philosophie** ».
Elle aura lieu **samedi 1<sup>er</sup> avril de 11h à 11h55, atelier du mouvement.**
J’y présenterai le logiciel que je développe depuis l’an dernier au sein de [Probesys](https://www.probesys.com/) : [Bileto](https://github.com/Probesys/bileto).
Il s’agit d’un logiciel de gestion de tickets et de parc informatique.
Le projet est encore jeune, mais on avance bien !
Ce sera l’occasion de faire le point sur où on en est et de parler un peu de communauté.

Ma seconde conférence se nomme « **Jouons avec les flux Web** ».
Elle aura lieu **dimanche 2 avril de 13h à 13h55, salle des cultures.**
Je la donnerai avec ma casquette Flus.
J’y présenterai trois prototypes de logiciels qui utilisent des flux Web : [Hitchhiker](https://flus.fr/carnet/hitchhiker-generateur-de-planets-statiques.html) (un planet), [Flus Actualités / Hermès](https://flus.fr/carnet/flus-actualites.html) (une alternative collaborative à Google News) et [Loquace](https://flus.fr/carnet/loquace-un-agregateur-social.html) (un réseau social).
L’objectif est de montrer d’autres manières d’utiliser les flux Web que les traditionnels agrégateurs.

Le reste du weekend, je serai présent majoritairement sur le stand de Framasoft, et un peu sur le stand de Probesys.
N’hésitez pas à venir faire coucou !