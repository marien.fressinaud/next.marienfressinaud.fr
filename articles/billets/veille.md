---
title: Veille
date: 2022-01-27 20:49
---

Les pages de profil commencent à arriver dans Flus. C’était ce qu’il me manquait pour me mettre à partager ma veille.

Ma page de profil : [app.flus.fr/p/1670839367044869607](https://app.flus.fr/p/1670839367044869607). Vous y trouverez la liste de mes derniers liens partagés, ainsi que les collections dans lesquelles je les place. En ce moment, je range mes liens majoritairement dans la collection « [📣 Partages en vrac](https://app.flus.fr/collections/1722493917394130742) », qui n’a donc pas de thème particulier.

Les pages de profil disposent également d’un flux : [app.flus.fr/p/1670839367044869607/feed](https://app.flus.fr/p/1670839367044869607/feed) (à ajouter dans votre agrégateur de flux <abbr>RSS</abbr>).
