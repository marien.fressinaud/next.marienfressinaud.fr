---
title: Boop! 1.0
date: 2022-03-17 11:50
---

Il était temps de le faire : **je viens de sortir la version 1.0 de Boop!, mon générateur personnel de sites statiques.**

Si je dis qu’il était temps de le faire, c’est que je considère qu’il est terminé depuis octobre dernier. Terminé, cela signifie que je ne compte plus ajouter de grosses fonctionnalités et limiter le polissage au strict minimum. Les bugs et failles de sécurité continueront d’être corrigées. Comme je l’ai dit dans un article plus ancien : s’il manque des choses, ce sont pour d’autres besoins que les miens.

**Bref, Boop! est stable.**

Depuis la version 0.4 sortie en novembre 2019 (!), peu de choses ont été ajoutées :

- la possibilité d’exclure des articles de la page principale du blog (et de son flux) ;
- l’activation de toutes les extensions Markdown « [extra](https://python-markdown.github.io/extensions/extra/) » ;
- le support des hubs [Websub](https://fr.wikipedia.org/wiki/WebSub) pour les flux Atom ;
- la génération d’une page « [sitemap](https://fr.wikipedia.org/wiki/Plan_de_site) » ;
- l’ajout d’un <i lang="en">warning</i> si aucun <abbr>UUID</abbr> n’a été précisé dans la configuration ;
- et la vérification que plusieurs pages/articles ne s’écrasent pas les uns les autres.

S’agissant d’un projet pensé par et pour moi, je ne vous invite pas à l’utiliser : **les chances sont grandes pour qu’il ne vous convienne pas.** S’il vous prenait toutefois l’envie de jouer avec, la documentation devrait désormais être complète.

**Vous trouverez [le code et la documentation de Boop! sur Framagit.](https://framagit.org/marienfressinaud/boop)**