---
title: Lollypop
date: 2022-03-01 12:34
---

Comme je l’ai expliqué [précédemment](debrancher-internet.html), je passe plus de temps à travailler hors-ligne. Comme j’écoute également beaucoup de musiques, je redécouvre celles qui traînent sur mon disque dur. J’ai longtemps été satisfait par [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox) pour écouter ma musique, mais j’ai fait face à pas mal de bogues gênants en le rouvrant dernièrement. **On m’a suggéré de jeter un œil à [Lollypop](https://wiki.gnome.org/Apps/Lollypop) ; c’était exactement ce que je cherchais.**

Non content d’être moins bogué, son interface est plus intuitive et plus pratique. L’interface ne ressemble pas à un gestionnaire de base de données, mais **à une application dédiée à l’écoute de la musique.** La navigation se fait notamment par albums, artistes, voire par années ou genres de musique. Il y a également un système de suggestions ; j’apprécie tout spécialement la suggestion de l’« album du jour ».

Il y a toutefois parfois de petites frictions à l’utilisation comme des incertitudes sur comment accéder à l’écran de l’album actuellement joué par exemple. Le plus gênant à mon sens est la synchronisation initiale des musiques : l’interface reste vide tant que la synchro n’est pas terminée, ce qui peut durer plusieurs minutes. J’ai cru que Lollypop ne fonctionnait pas, jusqu’à apercevoir une (discrète) barre de chargement en bas de l’écran. **Mais malgré ces quelques petits défauts, je tiens à souligner la très grande qualité de Lollypop.**

Mention spéciale à son auteur qui [considère Lollypop comme « terminé »](https://linuxfr.org/users/gnumdk/journaux/eolie-le-petit-frere-de-lollypop), signe de [maturité](terminer-ses-projets.html) du projet. J’en ai profité pour lui faire un don de 10 €.

![Capture d’écran de Lollypop affichant une liste d’albums](images/lollypop.png)
