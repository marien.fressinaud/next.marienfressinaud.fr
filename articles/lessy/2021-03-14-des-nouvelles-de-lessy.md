date: 2021-03-14 14:43
title: Des nouvelles de Lessy
slug: des-nouvelles-de-lessy

Je n’ai pas donné beaucoup de nouvelles de [Lessy](https://lessy.yuzu.ovh) ces
derniers temps (le dernier article remonte à septembre 2018). Je profite donc
que le serveur ait cramé pour vous en retoucher deux mots.

Pour rappel, Lessy est un gestionnaire de temps destiné à vous aider à mieux
vous organiser en associant à vos tâches des indicateurs clairs sur ce que vous
avez de plus urgent à réaliser (ou abandonner) dans l’immédiat. Un service est
mis à disposition gratuitement sur [lessy.yuzu.ovh](https://lessy.yuzu.ovh) et le code est
[hébergé sur GitHub](https://github.com/lessy-community/lessy), le tout sous
[licence libre](https://github.com/lessy-community/lessy/blob/master/LICENSE).

## Incendie OVH

Suite à [l’incendie chez OVH](http://travaux.ovh.net/?do=details&id=49484&)
mercredi dernier, deux de mes serveurs ont été impactés : celui qui hébergeait
mes courriels, et celui qui hébergeait Lessy. J’ai remonté le premier
rapidement car il est critique pour mon quotidien (j’en ai parlé sur le [carnet
de Flus](https://flus.fr/carnet/2021-03-10-serveur-courriels-hs.html)). J’ai
trainé pour celui de Lessy car j’avais l’espoir de le récupérer… finalement le
verdict est tombé vendredi soir : c’est non. J’ai donc pris le temps de
remonter le serveur ce matin. Je disposais de backups des données à la date de
mardi 9 à 18h, un moindre mal.

Le serveur est toujours hébergé chez OVH, à Gravelines cette fois-ci.

## Et ensuite ?

Si vous utilisez Lessy, vous vous demandez peut-être si je prévois encore de
bosser dessus. C’est une question difficile.

En l’état, Lessy m’est en fait relativement peu utile. Je l’utilise
principalement pendant les périodes où j’ai la tête sous l’eau ; périodes qui
se font de plus en plus rares. Ma manière de gérer mes tâches a évolué et
j’arrive la plupart du temps à me contenter d’un bout de papier ou du tableau
blanc accroché dans ma cuisine. De plus, j’ai le sentiment qu’un outil a
tendance à figer l’évolution de mon organisation.

J’aime également pouvoir accéder à ce dont j’ai besoin de me souvenir sans
avoir à allumer le PC. Je diminue ainsi le risque de me faire happer par des
activités numériques.

Lessy, dans sa version actuelle, correspond en fait pas mal à ce que je
souhaitais avoir quand j’ai commencé son développement. J’ai bien des tas
d’idées, de choses à ajouter ou à changer, mais je crois qu’il s’agirait d’un
outil différent.

**Tout bien pensé, je crois que Lessy est terminé.**

## Dites-moi ce que vous en pensez

Je vois toujours des gens s’inscrire à Lessy, j’ignore d’où elles viennent. Il
y a à l’heure actuelle plus de 3 500 comptes créés et je suis extrêmement
surpris.

Je n’ai en revanche que (très) peu de retours des personnes qui l’utilisent et
j’ignore donc ce que vous en pensez. Aussi, il y a peut-être des
micro-changements à apporter qui me coûteraient peu de temps, mais qui vous
aideraient grandement au quotidien (je ne m’engage évidemment pas sur des
choses plus importantes).

Bref, si vous utilisez Lessy, n’hésitez pas à [me contacter](contact.html)
pour me faire vos retours, ça m’intéresse !
