---
title: Sonktionary
date: 2023-03-04 15:18
---

**Dans la catégorie « projets bêtes », je vous présente [Sonktionary](https://marienfressinaud.frama.io/sonktionary/).**
Il s’agit d’une table de sons (aussi appelée « <i lang="en">sound board</i> ») avec des mots issus du [Wiktionnaire](https://fr.m.wiktionary.org).
Le principe est simple : vous cliquez sur un mot, Sonktionary prononce le mot.

Là où il devient marrant, c’est qu’il est également capable de lire le « hash » de l’URL (i.e. ce qui se trouve après le `#`).
Par exemple, si vous ouvrez l’URL [marienfressinaud.frama.io/sonktionary/#on veut la retraite à soixante ans](https://marienfressinaud.frama.io/sonktionary/#on%20veut%20la%20retraite%20à%20soixante%20ans) et cliquez sur « Jouer », Sonktionary vous lira la phrase « on veut la retraite à soixante ans ».
Vous pouvez évidemment créer vous-même vos propres phrases en changeant les mots dans l’URL.
Les mots doivent être les mêmes que ceux de la page, mais vous pouvez les finir avec des virgules, des points et des point-virgules pour marquer les pauses.

Précision utile : **Sonktionary ne sert à rien[^1]** (j’avais seulement envie de m’amuser avec).

[^1]: C’est pourquoi c’est un chouette projet.

Pour accéder au code source, c’est [sur Framagit](https://framagit.org/marienfressinaud/sonktionary).
Vous pouvez également ouvrir le code source de la page : tout tient dans l’unique fichier `index.html` (à l’exception des fichiers sons).
Par contre, je n’ai pas fait d’effort sur la propreté du code ; vous êtes prévenu‧es.
