---
title: git revise
date: 2022-02-09 19:58
---

J’utilise très peu de surcouches ou de commandes additionnelles à [Git](https://git-scm.com/). Il y a toutefois une sous-commande qui m’est devenue indispensable : [`git revise`](https://github.com/mystor/git-revise/).

Si vous êtes du genre à réécrire l’historique de vos branches de travail — ce qui est une bonne chose à apprendre pour garder un historique clair —, vous devez connaître `git rebase`. Cette commande permet de réappliquer des commits par-dessus une nouvelle base. Je l’utilise principalement avec l’option `-i` qui permet de réécrire l’historique de manière interactive (pour fusionner ou éditer des commits à la volée par exemple).

`git revise` est sa petite sœur en plus intuitive et plus rapide. Elle permet notamment d’appliquer un patch sur un commit précis sans avoir à se fader la création d’un commit temporaire et la réorganisation des commits.