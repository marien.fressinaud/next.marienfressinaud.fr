---
title: Quitter Twitter
date: 2022-01-06 18:16
---

En ce début d’année 2022, j’ai décidé de fermer mon compte Twitter. J’avais beaucoup de bonnes raisons de le garder, mais j’en avais également pour le fermer.

Mon usage de Twitter consistait essentiellement à _retweeter_ des choses, sans apporter de contenu supplémentaire ; parfois à « aimer » un contenu ; très rarement à balancer un commentaire rapide. **Deux choses me gênaient là-dedans : la manière superficielle d’aborder le contenu, et le fait que mes partages ne bénéficient qu’aux personnes inscrites sur la plateforme.** En 2022, j’ai envie de prendre plus de temps pour digérer ce que je lis, écoute ou regarde. J’ai également envie de m’assurer que ce que je partage est accessible à toutes et tous : via le blog, [Flus](https://flus.fr), ou leurs flux <abbr>RSS</abbr> respectifs. Pour suivre ce blog, vous pouvez d’ailleurs [vous abonner à son flux](abonnement.html).

À cela s’ajoute que le contenu que je lisais sur Twitter était souvent négatif, voire énervé. Mon mental, déjà ébranlé par la période pandémique qui traîne en longueur, n’arrive plus à suivre. **Je préfère prendre soin de moi en coupant ce moteur à énergie négative.**

Mais ce qui aura eu raison de mon compte Twitter, c’est une fatigue accrue en fin d’année 2021 face à mon usage du numérique (j’en parle dans [mon article précédent](2022.html)). J’ai réalisé à contre-temps que je croulais sous les articles de presse — souvent déprimants — et j’ai fini par me désabonner de quelques sites d’actualités que je suivais, bien que j’en apprécie le contenu. Le problème était similaire sur Twitter, mais je pensais réussir à m’en prémunir en limitant le nombre de comptes auxquels j’étais abonné. Mais le réseau social est fourbe et incite constamment à suivre plus de monde : **lutter contre l’outil pour garder un fil d’actualité léger ne me convient plus.**

Certaines de mes critiques peuvent par ailleurs s’appliquer de manière similaire à Mastodon. J’ai songé y supprimer mon compte également, mais je me laisse encore quelque temps pour me décider. Il est plus probable que je passe d’abord le compte en privé afin d’expérimenter une autre forme d’échanges.

À côté de ça, je vais tout de même conserver les comptes Twitter de [Flus](https://twitter.com/flus_fr) et [FreshRSS](https://twitter.com/FreshRSS), car ils servent à faire connaître les projets. Je n’y consomme de toute façon que très peu de contenu, et me contenterai à l’avenir de relayer les informations accessibles ailleurs.

**Enfin, j’aime l’idée que quitter Twitter me redonne l’envie d’écrire sur ce blog, espace qui me correspond mieux en terme rédactionnel — je n’y ai aucune limite de taille, waouh ! — et de design — du fait-maison de A à Z ❤️**