Date: 2015-08-21 12:32
Title: Ray’s day : Celui pour qui sonnent les cloches.
Slug: rays-day-celui-pour-qui-sonnent-les-cloches
Tags: raysday
Summary: Le Ray’s day fut pour moi l’occasion de publier ma première nouvelle officiellement. J'ai pour l'occasion eu l'opportunité de publier aux côtés de mes copains de Framasoft. Quelques timides retours m'ont donné envie de poursuivre dans l'écriture :).

![Bannière Ray’s Day](images/raysday/raysday-basic-banniere-noir-transparent-1024x293.png)

On y est, le « *Ray’s day* » c’est aujourd’hui. Initié l’année dernière par l’auteur [Neil Jomunsi](http://page42.org), l’évènement regroupe [autour d’un site](https://raysday.net) les différentes initiatives visant à promouvoir la passion de la lecture. L’occasion est donnée d’écrire, de lire, de découvrir et, surtout, de partager. Si vous êtes curieux — et vous l’êtes assurément, non ? —, je vous donne rendez-vous sur le site [raysday.net](https://raysday.net).

En novembre dernier, souvenez-vous, je participais au NaNoWriMo. De cette expérience intense, je ne publiais qu’une liste d’intentions (*oui, je publierai l’histoire dans X semaines / mois / années*). Manque de temps, manque de confiance dans la qualité de mon écrit, prise de risques dans la rédaction qui me laissait dubitatif : je ne suis toujours pas prêt à publier le résultat. Un jour peut-être ?

En attendant, je profite de ce jour un peu spécial pour soumettre à votre lecture attentive un petit essai. J’ai décidé de partir d’une page blanche, d’une idée toute neuve — ou presque —, afin de ne me poser aucune limite. Le résultat ? Une nouvelle qui s’intitule *Celui pour qui sonnent les cloches*. Il s’agissait pour moi d’expérimenter une forme de récit un peu spéciale (vous comprendrez en lisant la nouvelle ;)) et j’ignore si le résultat est compréhensible ; je l’espère ! Il s’agit pour moi d’une première publication et j’implore donc votre indulgence ! :)

Je vous invite à télécharger le fichier PDF, le fichier EPUB (pour liseuses électroniques) ou encore les sources MarkDown de [mon histoire sur Framagit](https://git.framasoft.org/marienfressinaud/celui-pour-qui-sonnent-les-cloches). Celle-ci est publiée sous licence CC-BY. Si vous avez des retours à me faire, mon courriel pour l’occasion est [raysday@marienfressinaud.fr](mailto:raysday@marienfressinaud.fr).

De plus, l’opportunité m’a été donnée d’ajouter mon histoire à un recueil publié à l’occasion par les membres de Framasoft — dont je fais partie depuis janvier. Nous [proposons ainsi 8 histoires](http://framablog.org/2015/08/22/framasoft-fait-son-rays-day-avec-8-nouvelles-libres/) toutes (ou presque) inédites. Je me dois de les remercier même si la visibilité donnée à ma nouvelle (et donc la pression :)) risque d’être plus importante que ce que j’imaginais au début.

Je vous souhaite un bon Ray’s Day à tous !
