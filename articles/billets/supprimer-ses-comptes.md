---
title: Supprimer ses comptes
date: 2022-05-13 17:11
---

J’ai entrepris depuis quelques semaines de supprimer les comptes en ligne que je n’utilise plus.

Pour cela, je pioche dans ma liste d’identifiants stockés dans mon gestionnaire de mots de passe ([Bitwarden](https://bitwarden.com/)), je me connecte, je trouve l’endroit pour supprimer mon compte et je confirme.

Ça, c’est quand ça se passe bien. À la louche, je dirais que c’est ce qu’il se passe deux fois sur trois. Le reste du temps, il me faut :

- passer par un formulaire de contact ;
- fouiller dans les mentions légales pour dénicher une adresse de contact (idéalement le <abbr>DPO</abbr>) ;
- abandonner, tout simplement.

Lorsque je fais une demande, je mets à jour l’enregistrement dans Bitwarden pour préciser « (suppression demandée le XX/YY/ZZZZ) ». Souvent, je n’obtiens pas de réponse.

**On n’insistera jamais assez : si vous proposez ou imposez la création d’un compte en ligne, permettez également sa suppression dans des conditions satisfaisantes (un bouton dans une section « profil » ou « compte » fait l’affaire).**