---
title: La low-tech existe-t-elle dans le numérique ?
date: 2020-03-08 10:30
---

Je développe une gêne croissante vis-à-vis de la notion de « _low-tech_ » en
informatique : il me semble y déceler un biais inconfortable dans sa définition
même. Je précise que je parle en tant que développeur travaillant principalement
dans le Web. Cet article est – volontairement – biaisé.

Pour comprendre l’origine de mon problème, il me faut commencer par définir ce
que j’entends par _low-tech_. De mon point de vue, **il s’agit avant tout d’une
notion qui s’oppose à celle de _high-tech_.** Si vous effacez cette dernière du
paysage, la _low-tech_ disparaît par la même occasion car elle perdrait son
sens, sa raison d’exister. Je parle ici de la notion et non pas de l’existence
« d’objets _low-tech_ » (une roue en bois par exemple) qui ne disparaitront pas
par magie. Il nous faudrait toutefois désormais définir ce qui ferait la
spécificité d’un numérique _high-tech_ pour en tirer ce qui s’y oppose. **Or le
numérique est intrinsèquement _high-tech_ car il se base sur des connaissances
et des techniques de pointe.** Le fonctionnement d’un ordinateur et déjà par
lui-même quelque chose de pointu ; que dire alors de sa fabrication ? de
celle de nos ordiphones ? et des milliers de kilomètres de câbles parcourant la
terre et les océans pour former l’Internet mondial ?

_Comment définir un terme – la low-tech numérique – si son existence même est
impossible ? [^1]_

[^1]: Sauf à vivre en Océania en 1984, évidemment.

Il est toutefois évident que certaines pratiques et outils numériques décrivent
une réalité _low-tech_, ne serait-ce qu’à travers la définition que leur donne
leurs concepteurs et conceptrices. On peut notamment citer le site du « [_low-tech
magazine_](https://solar.lowtechmagazine.com/) », tournant sur un serveur
alimenté en énergie solaire et qui passe hors-ligne lorsque le soleil vient à
se cacher trop longtemps. En moins extrême, Gauthier Roussilhe a [raconté la
transition de son site vers un site « _low-tech_ »](http://gauthierroussilhe.com/fr/posts/convert-low-tech).
Plus récemment, Geoffrey Dorne [a également listé quelques sites (à inspiration)
_low-tech_](https://graphism.fr/quel-avenir-pour-les-sites-low-tech/).
J’ai moi-même créé [un générateur « low-tech » de sites statiques](https://framagit.org/marienfressinaud/boop).

Une chose me frappe alors : le numérique _low-tech_ serait-il le Web « un point
zéro » des débuts ? de simples sites statiques à l’occasion d’un grand bond en
arrière dans nos usages ? C’est évidemment une manière très caricaturale de
voir les choses et il suffit de reprendre l’article de Geoffrey pour cela : il
y cite notamment Wikipédia et un thème pour Wordpress. On reste évidemment sur
des systèmes très simples, mais on voit que la _low-tech_ n’est pas uniquement
réservée aux sites statiques ; une part de « dynamique » y est autorisée.

Reste à déterminer ce qui rentre dans la case et ce qui n’y rentre pas. Une
application métier peut-elle être _low-tech_ ? Si je me base sur mon ressenti
j’ai envie de répondre par l’affirmative. Qu’en est-il d’un service de
_streaming_ de vidéos ? Ici, j’ai envie de répondre par un franc « non »… mais
si les vidéos sont optimisées au poil de cul, mises en cache et partagées sur
le réseau local au point que leur impact écologique ne constitue plus qu’une
ridicule broutille ?

_Ce n’est peut-être pas aussi simple…_

On voit toutefois sur cet exemple de site de _streaming_ que pour faire du
_low-tech_, j’ai dû user de l’argument d’un certain savoir-faire technique ;
d’une technique qui me permette de réduire l’impact écologique du site en
question. Je m’étais d’ailleurs fait cette réflexion en lisant l’article de
Gauthier qui expliquait avoir réduit de 75 % le poids des vidéos qu’il
hébergeait, soit un passage de quelque 14 Go à un poids final d’environ 3 Go.
Finalement, les autres optimisations qu’il avait pu faire par ailleurs ne
représentaient plus grand-chose comparé au gain fait sur les vidéos. On aurait
pu le questionner sur l’impact de la démarche elle-même – le temps de calcul
pour optimiser les images par exemple – comparé aux gains. Quant aux vidéos il
avait fait le choix de les conserver, au moins en partie. Pourtant, tout cela
ne semblait pas avoir été fait en vain ; l’article a même été abondamment
relayé autour de moi, comme pour me certifier qu’il s’agissait bien là d’un
article de référence dans le paysage de la _low-tech_.

De tout cela, j’en tirai la conclusion que **« faire de la _low-tech_ » n’est
pas tant lié au résultat final qu’à une démarche volontaire.**

Finalement, c’est Bertrand Keller qui l’exprimait le mieux [dans son article
« La Low-Tech, concept positif »](https://bertrandkeller.info/2019/06/25/low-tech-concept-positif/)
(la mise en gras est de moi) :

> Dans l’article ([Les tables de la Low (Tech)](https://bertrandkeller.info/2019/06/24/table-low-tech/)),
> j’ai repris la définition donnée par le [Low-Tech Lab](https://lowtechlab.org/),
> pour la questionner. Je n’ai pas vu dans la définition, de notion de perte
> d’usage. On parle d’utilité, d’accessibilité, de durabilité ; c’est-à-dire
> **un concept de but mais pas de perte d’usage** sous prétexte que ça consomme
> moins d’énergie.

Pour comprendre la _low-tech_, il faudrait donc prendre le problème par l’autre
bout : **pour un usage donné que je considère utile, comment puis-je le rendre
le plus accessible et durable possible ?**

_À ce point-là de mon article, je ne suis toujours pas aligné avec le terme
même de low-tech, mais sa finalité positive me parle bien._

Toutefois, quelque chose continue de me chiffonner et je vais l’illustrer avec
une petite anecdote. Lors du dernier [Snowcamp](https://snowcamp.io/fr/), une
conférence – Développement Zéro Déchet – a fait salle comble (c’est un
euphémisme tellement ça débordait de partout). Les intervenant·es n’abordaient
pas frontalement la notion de _low-tech_, mais vous conviendrez que quand on
parle de « zéro déchet », on n’en est quand même pas bien loin. Il et elle
expliquaient les étapes pour optimiser une application développée précédemment
en se basant sur des principes « zéro déchet ». Sauf que – et je ne sais pas si
ça a été décidé consciemment avec le souci d’illustrer – l’application avait
été tellement mal conçue à l’origine que je trouvais l’exercice totalement
aberrant. J’ai plutôt eu l’impression d’assister à un cours d’optimisation
assez basique que réellement une réflexion autour d’un hypothétique
« développement zéro déchet ».

La démarche n’en était pas moins intéressante, d’autant plus que c’est ce qui
m’a mis la puce à l’oreille qu’il pouvait y avoir un problème plus profond dans
cette histoire de _low-tech_ numérique. Finalement ne s’agirait-il pas d’une
forme de _green-washing_ pour développeur·ice en quête de sens dans son
métier ? **Sous couvert de vouloir alléger notre empreinte carbone, nous
commencerions (enfin) à développer nos sites de manière correcte et optimisée ;
de l’optimisation technico-politique en fin de compte.**

Cela cache par ailleurs un problème que je n’ai vu nulle part posé : de quel
impact concret parle-t-on ? Je n’en ai pas la réponse, mais si des études
existent sur le sujet je ne serais pas étonné que le gain du passage d’un site
dynamique à du statique soit ridiculement faible face au visionnage d’une vidéo
de chaton sur Youtube, lui-même minime face à la fabrication d’un terminal.
« Développer des sites statiques », bientôt dans les conseils du gouvernement
pour combattre le réchauffement climatique au côté de « faire pipi sous la
douche » et « trier ses courriels » !

_Mais ce serait considérer le problème uniquement d’un point de vue comptable._

On en revient doucement à l’éternel débat : les actions individuelles de faible
impact valent-elles le coût d’être menées ? J’ai tranché pour mon cas personnel
depuis un moment en considérant que c’est très justement à chaque individu d’y
apporter une réponse. Les encourager ne me semble toutefois pas idiot en cela
qu’une action individuelle reste souvent un premier pas facile au sein d’une
démarche politique plus large (ici, « réduire notre impact environnemental »).
**Cela ne doit pas occulter que les vrais changements surviendront à un niveau
systémique… mais est-ce qu’un ensemble d’individus ne peut pas former un
« système » en capacité de s’opposer à d’autres forces systémiques ?[^2]**

[^2]: J’avais initialement écrit « _qu’est-ce qu’un « système » si ce n’est un
  ensemble d’individus ?_ » On m’a très justement fait remarquer que cela
  occultait la présence d’autres « systèmes » comme les États ou Marchés.

Pour résumer tout ce que j’ai dit jusque-là, on a vu que la notion de
_low-tech_ en numérique n’a pas tellement de réalité « physique » (ou alors
peut-on parler de « _low-tech-on-high-tech_ » ?) Il s’agirait aujourd’hui
plutôt d’un mouvement d’individus adoptant une démarche politique volontaire
d’optimisation de leurs sites Internet afin de réduire leur impact écologique.
Ça peut sembler extrêmement réducteur comme vision et je rappelle qu’il s’agit
uniquement de la mienne, exprimée à travers mes propres biais. Connaître
les limites d’une telle démarche me semble toutefois important pour prendre
conscience qu’il ne s’agit que d’un outil parmi d’autres pour ne pas s’en
contenter. Cela peut également aider à répondre à des critiques.

_Vous ne sauverez certainement pas la planète avec des sites statiques, mais
vous aiguiserez vos engagements tout en rendant vos visiteur·heureuses[^3]._

[^3]: L’écriture inclusive permet également d’inventer de nouveaux type de jeu
  de mots. On est d’accord que ce n’est pas super lisible par contre !

Pour ma part, je ne pense pas / plus utiliser le terme de _low-tech_ car je
le trouve mal adapté à ce qu’il cherche à définir. Pour l’instant je lui
préfère la notion de « sobriété » qui a l’avantage de ne pas s’opposer
frontalement à la _high-tech_ (ce que le numérique _est_ intrinsèquement). Il
s’agit évidemment d’un choix personnel, et Bertrand – cité plus haut – s’est
aussi [questionné](https://bertrandkeller.info/2019/06/20/web-low-tech/) sur le
sujet en faisant le choix inverse. Cela n’enlève rien à l’utilité de cet
_outil_ qui cherche à questionner l’impact du numérique sur le monde
physique qui nous entoure. Il vient donc s’ajouter à ma besace, en compagnie de
la notion de logiciel libre qui questionne le concept de propriété. **Dans ces
deux cas, ce n’est pas tant leur finalité qui m’intéresse que les imaginaires
positifs qu’ils tentent de bâtir, ainsi que les cultures qui en émergent ;
comme des remèdes à un monde qui ne tourne parfois plus si rond.**
