---
title: IndieWeb
date: 2022-02-12 13:15
---

Ça faisait quelques années que je gardais [le site de l’IndieWeb](https://indieweb.org/) dans un coin. Parfois il disparaissait de mes signets, mais il y revenait systématiquement. Comme je passe plus de temps sur mon site ces derniers temps, j’ai enfin saisi l’occasion de me pencher dessus le weekend dernier.

Déjà, c’est quoi l’IndieWeb ? Voilà ma traduction de la définition donnée sur leur site :

> L’IndieWeb est une communauté de sites Web personnels individuels, connectés par des standards simples, basés sur les principes de posséder votre nom de domaine, de l’utiliser comme identité principale, de publier sur votre propre site (éventuellement en syndiquant ailleurs) et de posséder vos données.

**Plus concrètement, il s’agit d’une démarche qui vise à fédérer des sites Web personnels indépendants à travers des formats et des protocoles simples à mettre en place.** Par exemple, le protocole [Webmention](https://indieweb.org/Webmention) permet de notifier un site lorsque vous le mentionnez sur votre propre site ou sur un réseau social. Le mécanisme de réponse ou de « <i lang="en">likes</i> » n’est ainsi plus restreint à une plateforme en particulier mais est entièrement décentralisé.

**Pour démarrer, la page [<i lang="en">Getting started</i>](https://indieweb.org/Getting_Started) de leur wiki est plutôt bien faite,** bien que légèrement intimidante au premier abord. Voici les étapes que j’ai suivies :

1. publier mon propre site derrière un nom de domaine que je possède (ça, c’était bon 👍) ;
1. déclarer mon identité au format [`h-card`](https://indieweb.org/h-card) ;
1. relier mon site à mes comptes de réseaux sociaux à coup [de liens `rel=me`](https://indieweb.org/rel-me) ;
1. déclarer mon contenu au format [`h-entry`](https://indieweb.org/h-entry) et [`h-feed`](https://indieweb.org/h-feed) ;
1. syndiquer automatiquement mon contenu ailleurs (je pourrais éventuellement le faire vers Mastodon, mais je ne suis pas sûr de le vouloir ; par contre c’est le cas vers [Flus](https://app.flus.fr/collections/1697725238755346702)) ;
1. partager ma démarche (c’est maintenant fait !)

C’est un peu plus simple si vous utilisez un <abbr>CMS</abbr> comme Wordpress. Dans mon cas — un site statique généré à la main —, il a fallu plonger les mains dans le <abbr>HTML</abbr>. **Heureusement, les formats sont très simples à mettre en place : seulement quelques attributs `class` à ajouter par-ci par-là.**

La page ne suggère pas de mettre en place [Webmention](https://indieweb.org/webmention), mais ça me grattait et je m’en suis occupé ce matin grâce à [webmention.io](https://webmention.io/). Les échanges sur Mastodon autour du sujet m’y ont largement encouragé. Mon but est d’être tenu au courant — via un flux Atom généré par webmention.io — lorsque mon site est mentionné ailleurs. Je ne sais pas encore si j’afficherai les mentions directement ici par contre.

Maintenant se pose la question de l’intérêt d’avoir fait tout ça. Pour être honnête, ça ne me saute pas immédiatement aux yeux en l’absence d’un écosystème riche autour de ces technologies. **L’écosystème ne se mettra toutefois pas en place de lui-même, sans adoption par celles et ceux qui font le Web[^1].**

[^1]: Je le chuchote en fin de page, mais j’aimerais creuser ce qu’il est possible de faire avec [Flus](https://flus.fr). J’imagine déjà des choses avec les formats `h-entry` et `h-feed`, ou encore avec Webmention. Tout cela reste **hypothétique** car j’ai encore bien d’autres choses à faire avant !