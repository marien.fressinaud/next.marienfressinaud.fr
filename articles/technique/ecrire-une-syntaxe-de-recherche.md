---
title: Écrire une syntaxe de recherche
date: 2023-05-02 18:10
---

J’aime plutôt bien la syntaxe de recherche de GitHub.
Elle permet à la fois de rechercher à partir de texte « brut », tout en ciblant des propriétés particulières (ex. `is:open label:bug`).

Ça faisait un moment que j’avais envie de développer un parser pour ce type de syntaxe.
Et ça tombe bien, j’ai eu l’occasion récemment d’en développer un !

Je l’ai écrit pour [le projet Bileto](https://github.com/probesys/bileto) (un gestionnaire de tickets et de parcs informatiques).
J’en ai profité pour [documenter le fonctionnement du moteur de recherche](https://github.com/Probesys/bileto/blob/main/docs/developers/search-engine.md) à destination des développeurs et développeuses.
La documentation de la syntaxe, quant à elle, est directement intégrée à l’interface.

Le projet est écrit avec [Symfony](https://symfony.com/), et une partie du moteur de recherche dépend de l’<abbr>ORM</abbr> [Doctrine](https://www.doctrine-project.org/).
Ceci dit, il devrait être assez facilement adaptable.
Toute une partie est générique, tandis que la partie « génération des requêtes <abbr>DQL</abbr> » (langage propre à Doctrine) est très proche du <abbr>SQL</abbr>.
