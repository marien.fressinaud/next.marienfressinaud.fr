---
title: Publier un README sous forme de site Web
date: 2022-03-02 23:49
---

J’ai parlé l’autre jour de mon initiative de fichier [GDPR.txt](gdpr-txt.html). J’ai publié un site ce soir pour présenter tout ça : [gdpr-txt.org](https://gdpr-txt.org). J’ai saisi l’occasion pour réfléchir à la manière la plus simple que j’avais de publier ce site. **En trois mots : un fichier README, Pandoc et GitLab.**

Le fichier README contient le contenu de mon site au format Markdown. C’est donc un site simple qui ne contient qu’une seule page. Pour commencer, ça me suffit amplement.

Pour le mettre en ligne, j’ai besoin de le transformer en <abbr>HTML</abbr>. Il existe des tas de solutions à base de générateur de sites statiques, mais je voulais un truc encore plus simple. J’ai un petit faible pour [Pandoc](https://pandoc.org/), le couteau-suisse des convertisseurs de documents. La commande est simple :

```console
$ pandoc --output=index.html README.md
```

Pandoc va lire le fichier `README.md` et générer un fichier `index.html`. On peut également préciser un template à Pandoc, afin qu’il génère un <abbr>HTML</abbr> plus complet :

```console
$ pandoc --output=index.html --template=template.html README.md
```

Le fichier `template.html` n’est pas très compliqué à écrire[^1] :

[^1]: Je l’ai tout de même légèrement simplifié par rapport à [mon fichier d’origine](https://framagit.org/marienfressinaud/gdpr-txt/-/blob/main/website/template.html) pour des raisons de clarté.

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>GDPR.txt</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            html {
                color: #2b0e44;
                font-size: 24px;
                line-height: 1.3;
                background-color: #fefcfe;
            }

            main {
                max-width: 700px;
                margin-right: auto;
                margin-left: auto;
                padding-top: 5rem;
                padding-bottom: 5rem;
            }

            a {
                color: #8445bc;
            }

            a:hover {
                color: #793aaf;
            }
        </style>
    </head>

    <body>
        <main>
			$body$
        </main>
    </body>
</html>
```

Notez que j’ai décidé d’embarquer un peu de <abbr>CSS</abbr> directement dans mon template pour éviter de m’encombrer d’un fichier supplémentaire. Ce sera un choix à revoir si le style devient plus complexe.

Il ne reste plus qu’à mettre ce <abbr>HTML</abbr> en ligne. Mon dépôt de code se trouve sur [Framagit](https://framagit.org/), une instance de GitLab. Celui-ci met à disposition un système pour publier facilement des sites statiques en ligne : [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). Il suffit de créer un fichier `.gitlab-ci.yml` et de lui faire générer le site dans un répertoire nommé `public/` :

```yaml
---

pages:
  stage: deploy
  script:
    - apt-get update -y && apt-get install -y pandoc
    - mkdir public
    - pandoc --output=public/index.html --template=template.html README.md
  artifacts:
    paths:
      - public
  only:
    refs:
      - main
```

À partir de là, le site est automatiquement publié sur `marienfressinaud.frama.io/gdpr-txt`. Il ne me reste plus qu’à configurer mon nom de domaine pour le faire pointer vers Framagit. Mon enregistrement <abbr>DNS</abbr> ressemble à ça chez [Gandi](https://gandi.net) :

```dns
@ 10800 IN ALIAS marienfressinaud.frama.io.
_gitlab-pages-verification-code 10800 IN TXT "gitlab-pages-verification-code=90dff6058cf863ecb357ab2279e144fb"
```

Pas d’inquiétude : les informations sont bien entendues fournies par Framagit[^2].

[^2]: Attention toutefois à l’enregistrement de vérification (le deuxième) qui est légèrement erroné dans Framagit : il faut soit ajouter un point à la fin de `_gitlab-pages-verification-code.gdpr-txt.org`, soit virer la dernière partie (ce que j’ai moi-même fait). Il y a moins de risque d’erreur en passant par l’interface graphique de Gandi (pour une fois).

Je suis plutôt content de cette solution. Elle ne révolutionne rien et reste technique, mais elle se met rapidement en place et m’a demandé peu de connaissances supplémentaires. J’aurais pu me passer de la partie <abbr>DNS</abbr>, voire également de Pandoc en écrivant directement le fichier <abbr>HTML</abbr> (ça m’arrive), mais j’aime le confort de rédaction du Markdown.