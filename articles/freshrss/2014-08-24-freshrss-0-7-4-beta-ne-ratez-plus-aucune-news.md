Date: 2014-08-24 15:35
Title: FreshRSS 0.7.4 (bêta) : ne ratez plus aucune news
Slug: freshrss-0-7-4-beta-ne-ratez-plus-aucune-news
Tags: php, freshrss
Summary: FreshRSS est le premier projet conséquent que j’ai porté. Initié en novembre 2012, la version 1.0 est sortie début 2015 seulement. J’annonçais peu après que je donnais les clés à la communauté pour reprendre le code [disponible sur GitHub](https://github.com/FreshRSS/FreshRSS). J’ai toutefois aujourd’hui toujours des idées pour l’améliorer.

Un mois est déjà passé depuis la dernière version et voici donc [une nouvelle bêta](https://github.com/FreshRSS/FreshRSS/releases/tag/0.7.4) qui apporte son lot de nouveautés qui, comme d’habitude, devraient vous plaire ;)

* Les notifications sont de la partie avec la gestion des notifications HTML5. Cette fonctionnalité est vraiment top pour savoir quand de nouveaux articles sont disponibles et marche parfaitement bien sous Firefox et FirefoxOS.
* Combiné à cela, le favicon devient dynamique et affiche le nombre d’articles non lus ! Joli et pratique :)
* Petite nouveauté qui est devenue urgente avec l’acquisition d’un FirefoxOS : la case à cocher pour rester connecté. Plus besoin de se reconnecter à chaque lancement du navigateur.
* La page de statistiques continue de s’étoffer en permettant de visualiser la répartition des articles par heure, jour et mois.
* La grosse nouveauté dont j’ai déjà parlé dans mon Shaarli, c’est le nouveau thème magnifique proposé par [Mister aiR](https://github.com/misterair/) déjà à l’origine d’un thème pour Shaarli et… Leed :) Grâce à lui j’ai d’ailleurs pu noter quelques limitations dans la création des thèmes, j’en parlerai dans la documentation.
* Tout un tas d’autres petites nouveautés dont des corrections de petits bugs (encore !).

Un mot sur l’avenir de FreshRSS maintenant. Comme déjà évoqué dans mon article "C’est l’histoire d’un break." (**edit** : cet article n’existe plus), le 14 septembre je termine mes études et mon contrat d’apprentissage pour entamer entre six à douze mois sabbatiques. Je compte bien consacrer le début à FreshRSS afin de sortir une version 1.0 avant la fin de l’année. Pour marquer le coup, je compte sortir la prochaine version stable (la 0.8) le jour de la fin de mon contrat. Comme cela, <del>je</del> on pourra ensuite commencer des gros travaux dont le système de plugins qui attend depuis longtemps.

Pour la version 0.8 je compte notamment sortir un nouveau système de mise à jour **complètement automatique**. J’ai déjà commencé à travailler dessus dans [une branche à part mais accessible](https://github.com/FreshRSS/FreshRSS/tree/411-update-system). Un [dépôt supplémentaire a été créé](https://github.com/FreshRSS/update.freshrss.org) pour gérer la partie serveur de ce nouveau système de mise à jour. Tout est fonctionnel actuellement mais pas trop testé. Je pense d’ailleurs que ce sera la dernière (voire la seule) grosse amélioration de cette version.

Je tiens encore (et toujours) à remercier les différents contributeurs très actifs (Alexandre, Alexis, Alwaysin, que des A !), les personnes qui parlent de FreshRSS, ceux qui soutiennent, qui font des propositions, qui envoient des mails, qui font des dons, etc. Je suis vraiment ébahi qu’autant de monde participe à FreshRSS :) J’en profite d’ailleurs pour rappeler, si vous voulez donner un coup de pouce, que j’ai ajouté le projet [sur AlternativeTo.net](http://alternativeto.net/software/freshrss/). J’étais un peu dubitatif de l’intérêt au début mais finalement j’ai pas mal de visites en provenance de ce site donc ça vaut le coup "de liker" le projet pour le faire connaître encore un peu plus ;)
